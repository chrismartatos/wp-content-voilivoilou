<?php
global $current_language_code;
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
		<title><?php  if(is_home())
					  {
					     wp_title('');
					   }
					   elseif(is_404()){
					     echo "404 (Page Not Found) - ";
					   }
					   elseif(is_page())
					   {
						  wp_title('');
					   } else {
						  wp_title('');
						} ?></title>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<?php get_template_part('elements/fav-icons'); ?>
		<?php wp_head(); ?>

		<!-- FONT AWESOME -->
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


<!-- Global site tag (gtag.js) - Google Ads: 871404809 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-871404809"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-871404809'); </script>

<!-- Event snippet for Convoilivoilou conversion page --> <script> gtag('event', 'conversion', { 'send_to': 'AW-871404809/IqDJCPWoyooBEImqwp8D', 'transaction_id': '' }); </script>

<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-84992965-1', 'auto');
ga('send', 'pageview');
</script>


<script type="application/ld+json">
	{"@context":"https:\/\/schema.org\/","@type":"Product","name":"VoiliVoilou","aggregateRating":{"@type":"AggregateRating","bestRating":5,"worstRating":4,"ratingValue":4.9,"reviewCount":30}}
	</script>
	
</head>
<body <?php body_class();?>>


<div id="preloader">
	<div class="wrapper">
		<div class="circle">
		<?php get_template_part('elements/logo-circle'); ?>
		</div>
	</div>
</div>

<?php
	$form_option = get_field("form_option");
	$padding_option = get_field("padding_options");

	$bg_color_content = get_field('background_color_content');
	$bg_color_wrapper = "";

	 if($bg_color_content=="White")
	 {
		 $bg_color_wrapper = " bg-white";
		 }elseif($bg_color_content=="Grey")
		 {
			 $bg_color_wrapper = " bg-grey";
			 }elseif($bg_color_content=="LightGrey")
			 {
				 $bg_color_wrapper = " bg-lgrey";
				 }
?>


<div id="page" class="<?php if($form_option == "Yes"){ echo "form-opened"; }elseif($form_option == "Remove"){ echo " form-removed"; } if($padding_option == "Yes"){ echo " remove-padding"; }?> <?php echo $bg_color_wrapper; ?>">



<?php
	/*--------------------------------------------------------
				Header
	--------------------------------------------------------*/
?>
	<header id="header" class="<?php if( is_front_page() ) { echo "home-nav"; } else { echo "sub-nav"; } ?>">
		<div class="wrap-header cf">
			<a class="v-logo" href="<?php echo home_url(); ?>" title="VoiliVoilou">VoiliVoilou</a>

			<nav id="main-nav">
				<?php
					wp_nav_menu( array(
						'menu' => 'Main Menu FR',
						'container'=> ''
					));
				?>
			</nav>

			<nav id="mobile-nav">
				<a id="hamburger" class="hamburger" href="#">
					<span class="line line-1"></span>
					<span class="line line-2"></span>
					<span class="line line-3"></span>
				</a>
			</nav>
		</div>
	</header>


<?php
	/*--------------------------------------------------------
				Mobile nav
	--------------------------------------------------------*/
?>
	<div id="mobile-fixed-nav">
		<button id="close-mobile-menu" onclick="$('#hamburger').click();"></button>
		<nav class="nav-wrap">
			<?php
				wp_nav_menu( array(
					'menu' => 'Mobile Nav FR',
					'container'=> ''
				));
			?>
		</nav>
		<?php get_template_part('elements/social-icons'); ?>
	</div>
