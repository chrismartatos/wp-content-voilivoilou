<?php get_header(); ?>

<section id="intro-header" class="cf sizeLarge">
	<h1 class="page-title">404 Error</h1>
</section>

<div id="content" class="page-<?php print $post->post_name; ?>">
	
	
	<section class="voili-feat remove-padding-bottom">
		<h2 class="desc">Ooops...we couldn't find what you're looking for. <br>Use the following links to navigate through our new site.</h2>
	</section>	
		
	<div class="voili-feat remove-padding-top">
		<div class="entry-content">
		<p style="text-align: center;"> <a href="/?page_id=5">Home</a> • <a href="/?page_id=8">Destinations</a> • <a href="/?page_id=19">Contact</a> </p>
		</div>
	</div>
	
</div><!--END #content -->

<?php get_footer(); ?>
