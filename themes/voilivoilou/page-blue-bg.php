<?php
/*
Template Name: Template - Blue Background
*/
?>

<?php get_header(); ?>	

<?php 
$blu_bg = get_field('blue_background');	
$blu_bg_src = $blu_bg['url'];

$desc_pos = get_field('desc_position');	
?>

<div class="blu-bg bg-fixed" <?php echo 'style="background-image:url('.$blu_bg_src.');"'; ?>>
	<?php include('elements/banner.php'); ?>
	
	<div id="content" class="page-<?php print $post->post_name; ?>">
		
		<?php
			if($desc_pos == "Top")
			{
				include('elements/description.php');
			}	
		?>
		
		<?php if( have_posts() ): the_post(); ?>
		
		<?php
			//Flexible content 
			include('elements/flexible-content.php'); 
			
			if($desc_pos == "Bottom")
			{
				include('elements/description.php');
			}
				
		?>
		<?php else: ?>
			<p>Sorry, this page not longer exists.</p>
		<?php endif; ?>
		
	</div><!--END #content -->	
</div>

<div class="voili-feat">
	<div class="entry-content">
		<?php the_content(); ?>
	</div>
</div>

<?php get_footer(); ?>
