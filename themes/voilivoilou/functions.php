<?php
//WPML CURRENT
global $current_language_code;
global $blank_gif;

//SVG global
global $v_svg;
global $c_svg;
global $bm_svg;

/*-------------------------------------------------------------------------------
	Content
---------------------------------------------------------------------------------
	1. Documentation
	2. Remove code from head
	3. Register Menus
	4. Basics
	5. Register Scripts
	6. Register CSS
	7. Bug Fix for shortcodes
	8. Add Custom Post Type
	9. Add Taxonomies
    10. Shortcode Projects
    11. Add custom class to body for yachts template
================================================================================*/


/*-----------------------------------------------------------------------------------------------------
	Current Language
-----------------------------------------------------------------------------------------------------*/
$current_language_code = apply_filters( 'wpml_current_language', null );
$blank_gif = get_stylesheet_directory_uri().'/images/blank.gif';


/*--------------------------------------------------------------------------------------------------------------------------
	1. Documentation
--------------------------------------------------------------------------------------------------------------------------*/
function theme_doc()
{
	include("admin/admin-shortcodes.php");
}

function addCustomMenuItem()
{
	add_theme_page('Documentation', 'Documentation', 'edit_posts', 'theme-doc', 'theme_doc');
}
add_action('admin_menu', 'addCustomMenuItem');



/*-----------------------------------------------------------------------------------------------------
	2. Remove code from head
-----------------------------------------------------------------------------------------------------*/
add_action('widgets_init', 'my_remove_recent_comments_style');

function my_remove_recent_comments_style()
{
	global $wp_widget_factory;
	remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}

remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );

/*-----------------------------------------------------------------------------------------------------
	3. Menus - Options
-----------------------------------------------------------------------------------------------------*/
register_nav_menus(array(
	'main_nav' => 'Main Menu',
	'footer_nav' => 'Footer Menu',
	'mobile_nav' => 'Mobile Menu'
));



/*-----------------------------------------------------------------------------------------------------
	4. Basics
-----------------------------------------------------------------------------------------------------*/
// Enable The Post Thumbnail
add_theme_support( 'post-thumbnails' );

add_theme_support( 'title-tag' );

// Enable Widgets
//register_sidebar();

//SVG
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');



/*-----------------------------------------------------------------------------------------------------
	5. JS - footer - Register Scripts
-----------------------------------------------------------------------------------------------------*/
function front_end_scripts()
{
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js', array(), null, false );
    wp_register_script( 'front-end-plugins', get_template_directory_uri() . '/js/plugins.js', array( 'jquery' ), null, true );
    wp_register_script( 'front-end-slick', get_template_directory_uri() . '/js/slick.min.js', array( 'front-end-plugins' ), null, true );
    wp_register_script( 'front-end-script', get_template_directory_uri() . '/js/main.js', array( 'front-end-slick' ), null, true );
    wp_enqueue_script( 'front-end-script' );
}

add_action( 'wp_enqueue_scripts', 'front_end_scripts' );



/*-------------------------------------------------------------------------------------------------------
	7. Remove p from shortcodes(Fix bug)
--------------------------------------------------------------------------------------------------------*/
function wpex_clean_shortcodes($content){
$array = array (
    '<p>[' => '[',
    ']</p>' => ']',
    ']<br />' => ']'
);
$content = strtr($content, $array);
return $content;
}
add_filter('the_content', 'wpex_clean_shortcodes');


/*-------------------------------------------------------------------------------------------------------
		Mobile FN - Lazyload
--------------------------------------------------------------------------------------------------------*/

/*Reutn class*/
function lazy_load_class()
{
	if(wp_is_mobile())
	{
		return " lazyload-init";
	} else {
		return " desktop-machine";
	}
}

/*Return Image src*/
function lazy_load_src($image_src)
{
	$blank_gif = get_template_directory_uri().'/images/blank.gif';

	if(wp_is_mobile())
	{
		return $blank_gif;
	} else {
		return $image_src;
	}
}



/*-------------------------------------------------------------------------------------------------------
		IFRAMES
--------------------------------------------------------------------------------------------------------*/
add_filter('embed_oembed_html', 'my_embed_oembed_html', 99, 4);
function my_embed_oembed_html($html, $url, $attr, $post_id) {
  return '<div class="video-container">' . $html . '</div>';
}


/*-------------------------------------------------------------------------------------------------------
		INCLUDES - folder: /functions/
--------------------------------------------------------------------------------------------------------*/

/****-> CUSTOM POST TYPES
-----------------------------------------------*/
include('functions/custom-post-types.php');

/****-> SHORTCODES
-----------------------------------------------*/
include('functions/shortcodes.php');

/****-> THEME SETTINGS
-----------------------------------------------*/
include('functions/theme-settings.php');

/****-> GALLERIES
-----------------------------------------------*/
include('functions/galleries.php');



/*-------------------------------------------------------------------------------------------------------
		Add lazy load
--------------------------------------------------------------------------------------------------------*/
function add_lazyload($content)
{
    $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
    $dom = new DOMDocument();
    @$dom->loadHTML($content);

    // Convert Images
    $images = [];

    foreach ($dom->getElementsByTagName('img') as $node) {
        $images[] = $node;
    }

    foreach ($images as $node)
    {
        $fallback = $node->cloneNode(true);

        $oldsrc = $node->getAttribute('src');
        $node->setAttribute('data-original', $oldsrc );
        $newsrc = get_template_directory_uri() . '/images/blank.gif';
        $node->setAttribute('src', $newsrc);

        $oldsrcset = $node->getAttribute('srcset');
        $node->setAttribute('data-srcset', $oldsrcset );
        $newsrcset = '';
        $node->setAttribute('srcset', $newsrcset);

        $classes = $node->getAttribute('class');
        $newclasses = $classes . ' lazyload-init lazy-hidden';
        $node->setAttribute('class', $newclasses);

        $noscript = $dom->createElement('noscript', '');
        $node->parentNode->insertBefore($noscript, $node);
        $noscript->appendChild($fallback);
    }

    $newHtml = preg_replace('/^<!DOCTYPE.+?>/', '', str_replace( array('<html>', '</html>', '<body>', '</body>'), array('', '', '', ''), $dom->saveHTML()));
    return $newHtml;
}

add_filter('the_content', 'add_lazyload');



//SVG GLOBAL VARS

$v_svg = '<svg version="1.1" id="boat-voiler" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="160px" height="160px" viewBox="0 0 160 160" enable-background="new 0 0 160 160" xml:space="preserve">
<path fill="#4D4D4D" d="M62.458,129.238h-0.882V30.613h0.882c14.156,0,35.985,21.179,35.985,49.322
	C98.442,106.2,81.628,129.238,62.458,129.238z M63.339,32.404v95.053c17.862-0.702,33.341-22.605,33.341-47.521
	c0-12.032-4.26-24.003-11.995-33.706C78.16,38.044,70.071,32.831,63.339,32.404z"/>
</svg>';

$c_svg = '<svg version="1.1" id="boat-catamaran" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="160px" height="160px" viewBox="0 0 160 160" enable-background="new 0 0 160 160" xml:space="preserve">
<path fill="#4D4D4D" d="M120.493,89.909v-0.883h-25.03c5.407-7.422,8.849-17.519,8.849-28.279c0-23.109-17.951-40.504-29.592-40.504
	h-0.883v68.783H39.506v0.883c0,6.753,3.48,13.148,9.147,18.258h-9.147v0.882c0,15.764,18.917,29.592,40.483,29.592
	c23.11,0,40.504-17.951,40.504-29.592v-0.882h-10.529C116.562,102.389,120.493,95.438,120.493,89.909z M102.549,60.747
	c0,20.216-12.494,38-26.947,38.699V22.04C86.585,22.923,102.549,39.925,102.549,60.747z M79.989,136.876
	c-20.218,0-38-12.491-38.699-26.947h77.403C117.812,120.914,100.812,136.876,79.989,136.876z M107.09,108.167H51.309
	c-5.911-4.754-9.704-10.866-10.019-17.379h32.547v10.443h0.883c7.255,0,14.095-4.015,19.371-10.443h24.603
	C118.26,96.21,113.896,102.836,107.09,108.167z"/>
</svg>';

$bm_svg = '<svg version="1.1" id="boat-motor" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="160px" height="160px" viewBox="0 0 160 160" enable-background="new 0 0 160 160" xml:space="preserve">
<g>
	<path fill="#4D4D4D" d="M80.648,81.547h-1.057V19.32h1.057c10.068,0,22.842,14.646,22.842,31.111
		C103.49,67.007,92.814,81.547,80.648,81.547z M81.705,21.499v57.896c10.597-0.826,19.675-14.007,19.675-28.963
		C101.38,35.095,89.869,22.528,81.705,21.499z"/>
	<path fill="#4D4D4D" d="M81.708,142.68h-1.056c-10.071,0-22.846-14.655-22.846-31.122c0-16.575,10.677-31.115,22.846-31.115h1.056
		V142.68z M79.597,82.594c-10.601,0.827-19.681,14.008-19.681,28.964c0,15.343,11.514,27.912,19.681,28.943V82.594z"/>
	<path fill="#4D4D4D" d="M81.199,82.053h-62.23V81c0-10.071,14.654-22.843,31.122-22.843c16.572,0,31.108,10.671,31.108,22.843
		V82.053z M21.147,79.943h57.901c-0.828-10.598-14.004-19.677-28.958-19.677C34.749,60.267,22.178,71.779,21.147,79.943z"/>
	<path fill="#4D4D4D" d="M111.206,103.832c-16.57,0-31.105-10.673-31.105-22.84v-1.056h62.228v1.056
		C142.328,91.061,127.674,103.832,111.206,103.832z M82.251,82.048c0.828,10.597,14.002,19.673,28.955,19.673
		c15.342,0,27.913-11.511,28.942-19.673H82.251z"/>
</g>
</svg>';

/*-----------------------------------------------------------------------------------------------------
	11. add class for custom template yachts
-----------------------------------------------------------------------------------------------------*/

if ( !function_exists( 'catch_body_classes' ) ) {

    function catch_body_classes( $classes ) {
        if ( is_page_template( 'single-yachts.php' )) {
            $classes[] = 'yachts-template-default';
            $classes[] = 'single-yachts';
        }

        return $classes;
    }

    add_filter( 'body_class', 'catch_body_classes' );
}
