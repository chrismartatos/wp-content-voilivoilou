<?php
global $current_language_code;
?>

<?php
	//Pagination arrows
	get_template_part('elements/pagination-arrows');
?>

<?php
	/*--------------------------------------------------------
				yachts slideshow
	--------------------------------------------------------*/

	//Activate slideshow
	$yachts_active_slideshow = get_field('yachts_slideshow');

	//Choose options
	$yachts_slideshow_options = get_field('yacht_slideshow_options');

	//Custom title
	$yachts_slideshow_title = get_field('carousel_title');

	//Global title
	$y_title = ($current_language_code=="fr")? 'Nos Bateaux': 'Boats';


	if(!is_front_page()):
	if($yachts_active_slideshow == "Yes"):
	?>

	<section class="voili-feat bg-cover bg-grey">

		<?php if(!empty($yachts_slideshow_title)): ?>
			<h3 class="feat-title"><?php echo $yachts_slideshow_title; ?></h3>
		<?php else: ?>
			<h3 class="feat-title"><?php echo $y_title; ?></h3>
		<?php endif; ?>


		<?php
			if($yachts_slideshow_options=="Show all")
			{
				echo do_shortcode('[yachts]');
			}


			if($yachts_slideshow_options=="Choose yachts")
			{
				get_template_part('elements/carousel-yachts-choose');
			}


			if($yachts_slideshow_options=="Choose category")
			{
				get_template_part('elements/carousel-yachts-category');
			}
		?>

	</section>

	<?php endif; ?>
	<?php endif; ?>

<?php
	if(!is_front_page()):
	/*--------------------------------------------------------
				Testimonials options - page ACF
	--------------------------------------------------------*/
	$testimonials_option = get_field('testimonials_option');

	if($testimonials_option == "Yes"):
?>

	<section class="voili-feat bg-cover bg-fixed bg-white map-bg blue-map-bg" style="padding-bottom: 0;">

		<h3 class="feat-title"></h3>
		<?php echo do_shortcode('[clients]'); ?>
	</section>

<?php
	endif;
	endif;
?>


<?php
	/*--------------------------------------------------------
				Gallery option - page ACF
	--------------------------------------------------------*/
	$gal_option = get_field('gallery_option');

	if(!is_front_page()):
	if($gal_option == "Yes"):
?>

	<section class="voili-feat">
	<?php
		$p_opt_title = get_field('gallery_title_option');
		$p_opt_content = get_field('photo_gallery_option');
	?>
		<h3 class="feat-title"><?php echo $p_opt_title; ?></h3>
		<?php echo $p_opt_content; ?>
	</section>

<?php
	endif;
	endif;
?>


<?php
	/*--------------------------------------------------------
				BUTTON
	--------------------------------------------------------*/
?>
<div id="inquries" class="voili-feat cf bg-white">
	<a id="contact-button">
		<span class="fa fa-angle-down arrow"></span>
		<div class="vcenter-outer">
			<div class="vcenter-inner">
			<?php

			if($current_language_code=="fr")
			{
				echo 'Devis Gratuit';
			}
			elseif($current_language_code=="en")
			{
				echo 'Get a quote';
			}elseif($current_language_code=="es")
			{
				echo 'Solicitar Presupuesto';
			}
			?>
			</div>
		</div>
	</a>
</div>

<?php
	/*--------------------------------------------------------
				FORM
	--------------------------------------------------------*/
	$c_t = '';
?>
<section id="inquiries-form" class="voili-feat bg-cover bg-fixed cf bg-blue">
	<div class="wrap cf">
	<h3 class="feat-title"><?php echo $c_t; ?></h3>
	<div class="content">
		<div class="container">
		<?php
			if($current_language_code=="fr")
			{
				echo do_shortcode('[contact-form-7 id="240"]');
			}
			elseif($current_language_code=="en")
			{
				echo do_shortcode('[contact-form-7 id="3210"]');
			}elseif($current_language_code=="es")
			{
				echo do_shortcode('[contact-form-7 id="6777"]');
			}
		?>
		</div>
	</div>
	</div>
</section>


<?php
	/*--------------------------------------------------------
				FOOTER
	--------------------------------------------------------*/
?>
<footer id="footer">
	<div class="inner-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
				<nav id="footer-nav" class="cf">
					<?php
						wp_nav_menu( array(
							'menu' => 'Footer Nav FR',
							'container'=> ''
						));
					?>
				</nav>
				</div>
			</div>
		</div>
	</div>

	<div class="inner-bottom">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
					<div id="contact-info">
					<?php the_field('contact_info', 'option'); ?>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					<?php get_template_part('elements/social-icons'); ?>

					<?php
					$footer_message_fr = 'Votre agence de location de bateaux avec ou sans skipper pour voguer dans les îles grecques. VoiliVoilou';
					$footer_message_en = 'Votre agence de location de bateaux avec ou sans skipper pour voguer dans les îles grecques. VoiliVoilou';
					$footer_message = ($current_language_code=="fr")?  $footer_message_fr : $footer_message_en;
					?>

					<div class="footer-message">
						<p><?php echo $footer_message; ?></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
					<div id="copyrights">
						<?php
							$copyrights = ($current_language_code=="fr")? 'Tous droits réservés. ' : 'All rights reserved. ';

							echo '<p>© VoiliVoilou '.date("Y").' '.$copyrights.'<br>Design <a target="_blank" title="StudioJugi Web Design - Award Winning Designer" href="http://studiojugi.com/" rel="nofollow">StudioJugi</a></p>';
						?>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					<div id="legal-link">
						<?php
							$lg_t = ($current_language_code=="fr")? 'Mentions Légales' : 'Legal Notice';
							$lg_l = ($current_language_code=="fr")? 'mentions-legales': 'legal-notice';
						?>

						<p><a href="<?php echo esc_url( get_permalink( get_page_by_title($lg_t) ) ); ?>"><?php echo $lg_t; ?></a> | <span style="position: relative; left:4px;"><a href="https://www.paypal.me/voilivoilou" target="_blank" rel="nofollow"><img src="/wp-content/uploads/2017/01/visa-mastercard-paypal3.png" alt="paypal"></a></span><span style="position: relative; top:4px;"><a href="https://letsencrypt.org/" target="_blank" rel="nofollow"><img src="/wp-content/uploads/2017/12/lets-encrypt-white2.png" width="105" alt="lets encrypt"></a></span></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

</div><!--END #page-->

<?php wp_footer(); ?>

<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/077003fd53281eee05a730029/cc1b028ec36e2f20c2fbcf33d.js");</script>


<noscript><div class="no-js-msg">You have to enable javascript in your browser settings if you want to view this site</div></noscript>
<!-- social share -->
<script async>(function(s,u,m,o,j,v){j=u.createElement(m);v=u.getElementsByTagName(m)[0];j.async=1;j.src=o;j.dataset.sumoSiteId='297af3b339956dcb8786ee443c807bca11da733b0500c2582f39176d3cc4b915';v.parentNode.insertBefore(j,v)})(window,document,'script','//load.sumo.com/');</script>
</body>
</html>
