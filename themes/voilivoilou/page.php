<?php get_header(); ?>
	<!--VoiliPage-->
<?php include('elements/banner.php'); ?>
<?php

?>
<div id="content" class="page-<?php print $post->post_name; ?>">

	<?php if( have_posts() ): the_post(); ?>

	<?php include('elements/description.php'); ?>


	<?php
		//Flexible content
		include('elements/flexible-content.php');
	?>


	<?php else: ?>
		<p>Sorry, this page not longer exists.</p>
	<?php endif; ?>

</div><!--END #content -->

<?php get_footer(); ?>
