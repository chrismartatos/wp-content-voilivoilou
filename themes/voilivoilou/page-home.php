<?php
/*
Template Name: Template - Home
*/
?>

<?php get_header(); ?>

<?php
	//Hero slideshow
	include('elements/hero-slideshow.php');
	if(wp_is_mobile()){

	} else {
		//include('elements/video-fullscreen.php');
	}
	?>
<div id="search-results-booking-manager" class="booking-manager-home" style="margin: 70px 0 0 0; padding-top: 2rem;">
	<?php the_content(); ?>
</div>
<div id="content" class="page-<?php print $post->post_name; ?>" style="padding-top: 0;">

	<section class="voili-feat">
	<?php
		$d_title = get_field('home_destinations_title');
		$d_content = get_field('home_destinations_content');
	?>
		<h3 class="feat-title"><?php echo $d_title; ?></h3>
		<?php echo $d_content; ?>
	</section>

	<section class="voili-feat bg-white">
	<?php
		$b_title = get_field('home_boats_title');
		$b_content = get_field('home_boats_content');
	?>
		<h3 class="feat-title"><?php echo $b_title; ?></h3>
		<?php echo $b_content; ?>
	</section>

	<section class="voili-feat bg-grey">
	<?php
		$s_title = get_field('home_skippers_title');
		$s_content = get_field('home_skippers_content');
	?>
		<h3 class="feat-title"><?php echo $s_title; ?></h3>
		<?php echo $s_content; ?>
	</section>

	<section class="voili-feat">
	<?php
		$p_title = get_field('home_photos_title');
		$p_content = get_field('home_photos_content');
	?>
		<h3 class="feat-title"><?php echo $p_title; ?></h3>
		<?php echo $p_content; ?>
	</section>

	<section class="voili-feat bg-cover bg-fixed bg-white map-bg blue-map-bg" style="padding-bottom:0;">
	<?php
		$c_title = get_field('home_clients_title');
		$c_content = get_field('home_clients_content');
	?>
		<h3 class="feat-title"><?php echo $c_title; ?></h3>
		<?php echo $c_content; ?>
	</section>


</div><!--END #content -->

<?php get_footer(); ?>
