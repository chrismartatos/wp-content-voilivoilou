<?php

/*-------------------------------------------------------------------------------------------------------
	8. Custom Posts Type - Projects - By <///: https://github.com/chrismartatos/wp-add-cpt
--------------------------------------------------------------------------------------------------------*/
// flush_rewrite_rules();
/** DESTINATIONS **/
function cpt_destinations()
{
	  $singular = 'Destinations'; //Change Variables values

	  $plural = 'Destinations'; //Change Variables values

	  $slug = 'grece-iles-grecques-en-bateau'; //Change Variables values

	  $post_type = 'destinations'; //Change Variables values

	  $supports = array('title', 'excerpt', 'author', 'editor', 'thumbnail', 'custom-fields', 'revisions', 'page-attributes');


	  $labels = array(
		'name' => _x( $plural, 'post type general name'),
		'singular_name' => _x( $singular, 'post type singular name'),
		'add_new' => _x('Add New', strtolower( $singular ) ),
		'add_new_item' => __('Add New '. $singular),
		'edit_item' => __('Edit '. $singular ),
		'new_item' => __('New '. $singular ),
		'view_item' => __('View '. $singular),
		'search_items' => __('Search '. $plural),
		'not_found' =>  __('No '. $plural .' found'),
		'not_found_in_trash' => __('No '. $plural .' found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => $plural

	  );

	  $args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'rewrite' => Array('slug'=> $slug, 'with_front' => false),
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => true,
		'menu_icon' => 'dashicons-location-alt',
		'supports' => $supports

	  );

	  register_post_type( $post_type, $args );
}

add_action( 'init', 'cpt_destinations' );


/** SKIPPERS **/
function cpt_skippers()
{
	  $singular = 'Skippers'; //Change Variables values

	  $plural = 'Skippers'; //Change Variables values

	  $slug = 'vacances-en-grece-avec-skipper'; //Change Variables values

	  $post_type = 'skippers'; //Change Variables values

	  $supports = array('title', 'excerpt', 'author', 'editor', 'thumbnail', 'custom-fields', 'revisions', 'page-attributes');


	  $labels = array(
		'name' => _x( $plural, 'post type general name'),
		'singular_name' => _x( $singular, 'post type singular name'),
		'add_new' => _x('Add New', strtolower( $singular ) ),
		'add_new_item' => __('Add New '. $singular),
		'edit_item' => __('Edit '. $singular ),
		'new_item' => __('New '. $singular ),
		'view_item' => __('View '. $singular),
		'search_items' => __('Search '. $plural),
		'not_found' =>  __('No '. $plural .' found'),
		'not_found_in_trash' => __('No '. $plural .' found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => $plural

	  );

	  $args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'rewrite' => Array('slug'=> $slug, 'with_front' => false ),
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => true,
		'menu_icon' => 'dashicons-groups',
		'supports' => $supports

	  );

	  register_post_type( $post_type, $args );
}

add_action( 'init', 'cpt_skippers' );


/** YACHTS **/
function cpt_yachts()
{
	  $singular = 'Yacht'; //Change Variables values

	  $plural = 'Yachts'; //Change Variables values

	  $slug = 'location-bateau-grece-x'; //Change Variables values

	  $post_type = 'yachts'; //Change Variables values

	  $supports = array('title', 'excerpt', 'author', 'editor', 'thumbnail', 'custom-fields', 'revisions', 'page-attributes');


	  $labels = array(
		'name' => _x( $plural, 'post type general name'),
		'singular_name' => _x( $singular, 'post type singular name'),
		'add_new' => _x('Add New', strtolower( $singular ) ),
		'add_new_item' => __('Add New '. $singular),
		'edit_item' => __('Edit '. $singular ),
		'new_item' => __('New '. $singular ),
		'view_item' => __('View '. $singular),
		'search_items' => __('Search '. $plural),
		'not_found' =>  __('No '. $plural .' found'),
		'not_found_in_trash' => __('No '. $plural .' found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => $plural

	  );

	  $args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'rewrite' => Array('slug'=> $slug, 'with_front' => false ),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => true,
		'menu_icon' => 'dashicons-book',
		'supports' => $supports

	  );

	register_post_type( $post_type, $args );
}

add_action( 'init', 'cpt_yachts' );


/** CLIENTS Testimonials **/
function cpt_clients()
{
	  $singular = 'Client'; //Change Variables values

	  $plural = 'Clients'; //Change Variables values

	  $slug = 'clients'; //Change Variables values

	  $post_type = 'clients'; //Change Variables values

	  $supports = array('title', 'excerpt', 'author', 'editor', 'thumbnail', 'custom-fields', 'revisions', 'page-attributes');


	  $labels = array(
		'name' => _x( $plural, 'post type general name'),
		'singular_name' => _x( $singular, 'post type singular name'),
		'add_new' => _x('Add New', strtolower( $singular ) ),
		'add_new_item' => __('Add New '. $singular),
		'edit_item' => __('Edit '. $singular ),
		'new_item' => __('New '. $singular ),
		'view_item' => __('View '. $singular),
		'search_items' => __('Search '. $plural),
		'not_found' =>  __('No '. $plural .' found'),
		'not_found_in_trash' => __('No '. $plural .' found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => $plural

	  );

	  $args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'rewrite' => Array('slug'=> $slug, 'with_front' => false ),
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => true,
		'menu_icon' => 'dashicons-testimonial',
		'supports' => $supports

	  );

	  register_post_type( $post_type, $args );
}

add_action( 'init', 'cpt_clients' );


/*-------------------------------------------------------------------------------------------------------
	9. Register Taxonomies to Custom Post Types Projects
--------------------------------------------------------------------------------------------------------*/
add_action( 'init', 'create_taxonomies_yachts' );

function create_taxonomies_yachts()
{
    register_taxonomy(
        'category',
        'page',
        array(
            'labels' => array(
                'name' => 'Yacht Category',
                'add_new_item' => 'Add New Category',
                'new_item_name' => "New Category"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true,
            'show_in_nav_menus' => false,
            'meta_box_cb' => null,
            'show_admin_column' => true
        )
    );
}



add_action( 'init', 'create_taxonomies_skipper' );

function create_taxonomies_skipper()
{
    register_taxonomy(
        'skipper_category',
        'page',
        array(
            'labels' => array(
                'name' => 'Skipper Category',
                'add_new_item' => 'Add New Category',
                'new_item_name' => "New Category"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true,
            'show_in_nav_menus' => false,
            'meta_box_cb' => null,
            'show_admin_column' => true
        )
    );
}



