<?php

/*-------------------------------------------------------------------------------------------------------
		11. Galleries - Slideshows
--------------------------------------------------------------------------------------------------------*/

function voilivoilou_custom_gallery($string,$attr)
{
	$type = $attr['theme_gallery_type'];
	$posts = get_posts(array('include' => $attr['ids'],'post_type' => 'attachment', 'orderby' => 'post__in'));
	$output = '';
	$blank_gif = get_stylesheet_directory_uri().'/images/blank.gif';
	
	if($type == 'slideshow')
	{
		
		/**=> SLIDESHOW - 8 images
		--------------------------------------------------------------------------------------------------**/
		$output .= '<div class="container-fluid">';
		$output .= '<div class="voilivoilou-photo-gallery slideshow absolute-arrows arrows lazy-slides" data-wrap-slides="8">';
					
		foreach($posts as $imagePost)
		{
			$id = $imagePost->post_parent;
			$caption = $imagePost->post_excerpt;
			$img_l = wp_get_attachment_image_src($imagePost->ID, 'large');
			$img_f = wp_get_attachment_image_src($imagePost->ID, 'full');
			$img_m = wp_get_attachment_image_src($imagePost->ID, 'medium');
			$img_t = wp_get_attachment_image_src($imagePost->ID, 'thumbnail');
			
			$output .= '<div class="wrap slice-item col-lg-3 col-md-3 col-sm-3 col-xs-3">';
			$output .= '<a class="slide-lazyload-init item swipebox bg-cover" style="background-image:url('.$blank_gif.');" data-original="'.$img_t[0].'" href="'.$img_f[0].'" title="'.$caption.'" rel="image"></a>';
			$output .= '</div>';
		}
		$output .= '</div>';
		$output .= '</div>';
		
	}
	else if($type == 'slideshow_1')
	{
		
		/**=> SLIDESHOW - 1img MAP
		--------------------------------------------------------------------------------------------------**/	
		$output .= '<div class="voilivoilou-slideshow-one slideshow bullets lazy-slides">';
					
		foreach($posts as $imagePost)
		{
			$id = $imagePost->post_parent;
			$caption = $imagePost->post_excerpt;
			$img_l = wp_get_attachment_image_src($imagePost->ID, 'large');
			$img_f = wp_get_attachment_image_src($imagePost->ID, 'full');
			$img_m = wp_get_attachment_image_src($imagePost->ID, 'medium');
			
			$output .= '<div class="slide-item">';
			$output .= '<a class="slide-lazyload-init item swipebox bg-cover" style="background-image:url('.$blank_gif.');" data-original="'.$img_m[0].'" href="'.$img_f[0].'" title="'.$caption.'"></a>';
			$output .= '</div>';
		}
		$output .= '</div>';
	}
	else if($type == 'slideshow_2')
	{
		
		/**=> SLIDESHOW - 1img MAP
		--------------------------------------------------------------------------------------------------**/	
		$output .= '<div class="voilivoilou-slideshow-two slideshow absolute-arrows arrows">';
					
		foreach($posts as $imagePost)
		{
			$id = $imagePost->post_parent;
			$caption = $imagePost->post_excerpt;
			$img_l = wp_get_attachment_image_src($imagePost->ID, 'large');
			$img_f = wp_get_attachment_image_src($imagePost->ID, 'full');
			$img_m = wp_get_attachment_image_src($imagePost->ID, 'medium');
			/*<a class="item bg-cover" href="'.$img_f[0].'" title="'.$caption.'" rel="image"></a>*/
			$output .= '<div class="slide-item">';
			$output .= '<img class="slideshow-image" data-lazy="'.$img_l[0].'" alt="'.$caption.'">';
			$output .= '</div>';
		}
		
		$output .= '</div>';
	}

	return $output;
}

add_filter('post_gallery','voilivoilou_custom_gallery',10,2);


function voilivoilou_galleries()
{
	// the "tmpl-" prefix is required,
	// and your input field should have a data-setting attribute
	// matching the shortcode name
	?>
	<script type="text/html" id="tmpl-voilivoilou-photo-gallery-setting">
		<label class="setting">
			<span><?php _e('Gallery Type'); ?></span>
			<select data-setting="theme_gallery_type">
				<option value="default">Default Wordpress Gallery</option>
				<option value="slideshow">Slideshow - 8 images (Gallery)</option>
				<option value="slideshow_1">Slideshow - 1 image (Bullets)</option>
				<option value="slideshow_2">Slideshow - 1 image (Arrows, Bullets)</option>
			</select>
		</label>
	</script>

	<script>
		jQuery(document).ready(function()
		{
			// add your shortcode attribute and its default value to the
			// gallery settings list; $.extend should work as well...
			_.extend(wp.media.gallery.defaults, {
				theme_gallery_type: 'default'
			});

			// merge default gallery settings template with yours
			wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
				template: function(view)
				{
					return wp.media.template('gallery-settings')(view)
							 + wp.media.template('voilivoilou-photo-gallery-setting')(view);
				}
			});
		});
	</script>
	<?php
}
add_action('print_media_templates', 'voilivoilou_galleries');


/*-------------------------------------------------------------------------------
	Custom field for gallery
-------------------------------------------------------------------------------*/
function be_attachment_field_credit( $form_fields, $post ) 
{
	$form_fields['add_gallery_field_1'] = array(
		'label' => 'Title',
		'input' => 'text',
		'value' => get_post_meta( $post->ID, 'add_custom_class', true ),
		'helps' => 'Add title if necessary for slideshows',
	);
	
	return $form_fields;
}
add_filter( 'attachment_fields_to_edit', 'be_attachment_field_credit', 10, 2 );

function be_attachment_field_credit_save( $post, $attachment ) 
{
	if( isset( $attachment['add_gallery_field_1'] ) )
		update_post_meta( $post['ID'], 'add_custom_class', $attachment['add_gallery_field_1'] );
			
	return $post;
}
add_filter( 'attachment_fields_to_save', 'be_attachment_field_credit_save', 10, 2 );
