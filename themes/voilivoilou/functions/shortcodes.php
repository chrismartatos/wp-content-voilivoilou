<?php


/*-------------------------------------------------------------------------------------------------------
		10. Shortcode: DESTINATIONS
--------------------------------------------------------------------------------------------------------*/

function destinations_shortcode($atts)
{
	$output = '';

	//Options
    extract(shortcode_atts(array(
            'limit'    => '-1'
            ), $atts));

    global $post;

    //Query Options
    $args = array(
    'posts_per_page' => $limit,
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_type' => 'destinations',
    'post_parent' => 0
    );

    //Query for projects
    $get_articles = NEW WP_Query($args);
	$blank_gif = get_stylesheet_directory_uri().'/images/blank.gif';

	//Wrapper
	$output .= '<div id="destinations-feat" class="shortcode-voili">';
	$output .= '<div class="container">';
	$output .= '<div class="row">';

	//Loop
    while($get_articles->have_posts())
    {
        $get_articles->the_post();
        $img_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full");
		$feat_img = (!empty($img_src))? $img_src[0]: '';

		//'.lazy_load_class().'
		//style="background-image:url('.lazy_load_src($feat_img).');"

        $output .= '<article class="destination visible-animation col-lg-6 col-md-6 col-sm-6 col-xs-6">';
        $output .= '<div class="item cf">';
        $output .= '<div class="wrap hover-touch bg-cover lazyload-init" data-original="'.$feat_img.'">';
        $output .= '<a href="'.get_permalink().'" class="hover v-mask" data-url="'.get_permalink().'"></a>';
        $output .= '<div class="vcenter-outer">';
        $output .= '<div class="vcenter-inner">';
        $output .= '<h4><a href="'.get_permalink().'">'.get_the_title().'</a></h4>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</article>';
    };

    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';

    //Important: Reset wp query
    wp_reset_query();

    return $output;
}

add_shortcode('destinations', 'destinations_shortcode');



/*-------------------------------------------------------------------------------------------------------
		10. Shortcode: SKIPPERS
--------------------------------------------------------------------------------------------------------*/

function skippers_shortcode($atts)
{
	$output = '';

	//Options
    extract(shortcode_atts(array(
            'limit'    => '-1'
            ), $atts));

    global $post;

    //Query Options
    $args = array(
    'posts_per_page' => $limit,
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_type' => 'skippers',
    'hierarchical' => true
    );

    //Query for projects
    $get_articles = NEW WP_Query($args);
	$blank_gif = get_stylesheet_directory_uri().'/images/blank.gif';
	$active_id = get_the_ID();

	//Wrapper
	$output .= '<div id="skippers-feat" class="shortcode-voili">';
	$output .= '<div class="container">';
	$output .= '<div class="row">';

	//Loop
    while($get_articles->have_posts())
    {

        $get_articles->the_post();
        $img_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full");
		$feat_img = (!empty($img_src))? $img_src[0]: '';

        //'.lazy_load_class().'
        //style="background-image:url('.lazy_load_src($feat_img).')"

        $output .= '<article class="item visible-animation id-'. $post->ID .'">';
        $output .= '<a href="'.get_permalink().'" class="wrap bg-cover hover cat-'.get_field('categories').' lazyload-init" data-original="'.$feat_img.'"></a>';
        $output .= '<h4>'.get_the_title().'</h4>';
        $output .= '</article>';

    };

    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';

    //Important: Reset wp query
    wp_reset_query();

    return $output;
}

add_shortcode('skippers', 'skippers_shortcode');


/*-------------------------------------------------------------------------------------------------------
		10. Shortcode: CLIENTS
--------------------------------------------------------------------------------------------------------*/

function clients_shortcode($atts)
{
	global $current_language_code;

	$output = '';
	$reviews_title = '';
	$reviews_html = '';
	$reviews_link_title = '';


	if($current_language_code=='fr')
	{
		$reviews_title = 'Avis clients';
		$reviews_link = get_permalink(8136);
		$reviews_link_title = 'Voir tous les avis clients';
		$reviews_html = 'Note: <span itemprop="ratingValue">4.9</span> / 5
    calculé sur <span itemprop="reviewCount">30</span> avis';
	}
	elseif($current_language_code=='en')
	{
		$reviews_title = 'Customer Reviews';
		$reviews_link = get_permalink(8138);
		$reviews_link_title = 'See our customer reviews';
		$reviews_html = 'Rating: <span itemprop="ratingValue">4.9</span> / 5
    calculated on <span itemprop="reviewCount">30</span> reviews';
	}

	//Options
    extract(shortcode_atts(array(
            'limit'    => '8'
            ), $atts));

    global $post;

    //Query Options
    $args = array(
    'posts_per_page' => $limit,
    'orderby' => 'post_date',
    'post_type' => 'clients'
    );

    //Query for projects
    $get_articles = NEW WP_Query($args);

	//Wrapper
	$output .= '<div id="clients-feat" class="clients-feat shortcode-voili">';
	$output .= '<div class="container">';
	$output .= '<div class="row">';
	$output .= '<div class="slideshow bullets arrows absolute-arrows">';

	//Loop
    while($get_articles->have_posts())
    {
        $get_articles->the_post();
        $img_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full");
		$feat_img = (!empty($img_src))? $img_src[0]: '';
		$content = apply_filters("the_content",$post->post_content);

        $output .= '<div class="slide item" itemprop="review" itemscope itemtype="http://schema.org/Review">';
				$output .= '<span itemprop="itemReviewed" itemscope itemtype="http://schema.org/Place"><meta itemprop="name" content="Greece"></span>';
        $output .= '<div class="wrap" itemprop="reviewBody">'.$content.'</div>';
        $output .= '<div class="person" itemprop="author" itemscope itemtype="http://schema.org/Person"><h5 class="title" itemprop="name">'.get_the_title().'</h5></div>';
				$output .= '<span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><meta itemprop="ratingValue" content="5"></span>';
				$output .= '<span class="rating-stars">';
				$output .= '<i class="fa fa-star" aria-hidden="true"></i>';
				$output .= '<i class="fa fa-star" aria-hidden="true"></i>';
				$output .= '<i class="fa fa-star" aria-hidden="true"></i>';
				$output .= '<i class="fa fa-star" aria-hidden="true"></i>';
				$output .= '<i class="fa fa-star" aria-hidden="true"></i>';
				$output .= '</span>';
        $output .= '<div class="post-excerpt">'.get_the_excerpt().'</div>';
        $output .= '<time class="date">'.get_the_date('F Y').'</time>';
        $output .= '</div>';
    };

    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';
		$output .= '<div class="business-review">
  <article id="review-aggregate" itemscope itemtype="http://schema.org/Product">
    <h4 itemprop="name">'.$reviews_title.'</h4>

    <div class="rating">
      <span class="rating-stars">
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
      </span>
    </div>

    <p itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">'.$reviews_html.'</p>

  </article>
	<a href="'.$reviews_link.'" title="Google Reviews">'.$reviews_link_title.'</a>
	</div>';

    //Important: Reset wp query
    wp_reset_query();

    return $output;
}

add_shortcode('clients', 'clients_shortcode');




/*-------------------------------------------------------------------------------------------------------
		11. Shortcode: Customer reviews
--------------------------------------------------------------------------------------------------------*/

function customer_reviews_shortcode($atts)
{
	global $current_language_code;

	$output = '';
	$reviews_title = '';
	$reviews_html = '';
	$reviews_link_title = '';


	if($current_language_code=='fr')
	{
		$reviews_title = 'Avis clients';
		$reviews_link_title = 'Voir tous les avis clients';
		$reviews_html = 'Note: <span itemprop="ratingValue">4.9</span> / 5
    calculé sur <span itemprop="reviewCount">30</span> avis';
	}
	elseif($current_language_code=='en')
	{
		$reviews_title = 'Customer Reviews';
		$reviews_link_title = 'See our customer reviews';
		$reviews_html = 'Rating: <span itemprop="ratingValue">4.9</span> / 5
    calculated on <span itemprop="reviewCount">30</span> reviews';
	}

	$sidebar = '<div class="business-review">
<article id="review-aggregate" itemscope itemtype="http://schema.org/Product">
	<h4 itemprop="name">'.$reviews_title.'</h4>

	<div class="rating">
		<span class="rating-stars">
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
		</span>
	</div>

	<p itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">'.$reviews_html.'</p>

</article>
<a href="https://www.google.com/maps/place/Location+Voilier+Gr%C3%A8ce+-+VoiliVoilou/@37.9817369,23.7195615,18z/data=!4m7!3m6!1s0x0:0x5a03eaa1384ae0b6!8m2!3d37.9802655!4d23.7235499!9m1!1b1" target="_blank" title="Google Reviews">'.$reviews_link_title.'</a>
</div>';

	//Options
    extract(shortcode_atts(array(
            'limit'    => -1
            ), $atts));

    global $post;

    //Query Options
    $args = array(
    'posts_per_page' => $limit,
    'orderby' => 'post_date',
    'post_type' => 'clients'
    );

    //Query for projects
    $get_articles = NEW WP_Query($args);

	//Wrapper
	$output .= '<div class="shortcode-voili reviews-wrap">';
	$output .= '<div class="container clients-feat-grid">';
	$output .= '<div class="row">';
	$output .= '<aside class="col-xs-12 col-sm-12 clearfix" style="margin-bottom: 4rem;">'.$sidebar.'</aside>';


	//Loop
		while($get_articles->have_posts())
		{
				$get_articles->the_post();
				$img_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full");
		$feat_img = (!empty($img_src))? $img_src[0]: '';
		$content = apply_filters("the_content",$post->post_content);

				$output .= '<div class="col-xs-12 col-sm-4">';
				$output .= '<article class="item" itemprop="review" itemscope itemtype="http://schema.org/Review">';
				$output .= '<div class="item-wrap">';
				$output .= '<span itemprop="itemReviewed" itemscope itemtype="http://schema.org/Place"><meta itemprop="name" content="Greece"></span>';
				$output .= '<div class="wrap" itemprop="reviewBody">'.$content.'</div>';
				$output .= '<div class="person" itemprop="author" itemscope itemtype="http://schema.org/Person"><h5 class="title" itemprop="name">'.get_the_title().'</h5></div>';
				$output .= '<span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><meta itemprop="ratingValue" content="5"></span>';
				$output .= '<span class="rating-stars">';
				$output .= '<i class="fa fa-star" aria-hidden="true"></i>';
				$output .= '<i class="fa fa-star" aria-hidden="true"></i>';
				$output .= '<i class="fa fa-star" aria-hidden="true"></i>';
				$output .= '<i class="fa fa-star" aria-hidden="true"></i>';
				$output .= '<i class="fa fa-star" aria-hidden="true"></i>';
				$output .= '</span>';
				$output .= '<div class="post-excerpt">'.get_the_excerpt().'</div>';
				$output .= '<time class="date">'.get_the_date('F Y').'</time>';
				$output .= '</div>';
				$output .= '</article>';
				$output .= '</div>';
		};
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';


    //Important: Reset wp query
    wp_reset_query();

    return $output;
}

add_shortcode('reviews', 'customer_reviews_shortcode');




/*----------------------------------------------------------
		Shortcodes: Boats
----------------------------------------------------------*/
function boats_categories($atts) {

	global $c_svg;
	global $v_svg;
	global $bm_svg;
	global $current_language_code;
	$output = '';


	$v_1 = get_permalink(icl_object_id(4176, 'post', true));
	$v_2 = get_permalink(icl_object_id(4184, 'post', true));
	$v_terms = get_term_by('term_id', 7, 'category');


	$c_1 = get_permalink(icl_object_id(4193, 'post', true));
	$c_2 = get_permalink(icl_object_id(4191, 'post', true));
	$c_terms = get_term_by('term_id', 8, 'category');

	$bm_1 = get_permalink(icl_object_id(4199, 'post', true));
	$bm_2 = get_permalink(icl_object_id(4195, 'post', true));
	$bm_terms = get_term_by('term_id', 1, 'category');


	if($current_language_code=='fr')
	{
		$v_title = $v_terms->name;
		$c_title = $c_terms->name;
		$bm_title = $bm_terms->name;

		$v1_title = 'Avec ou sans skipper';
		$v2_title = 'Avec skipper ou équipage';

		$c1_title = 'Avec ou sans skipper';
		$c2_title = 'Avec skipper ou équipage';

		$bm1_title = 'Avec ou sans skipper';
		$bm2_title = 'Avec skipper ou équipage';
	}
	elseif($current_language_code=='en')
	{
		$v_title = 'Sail Yachts';
		$c_title = 'Catamarans';
		$bm_title = 'Motor Yachts';

		$v1_title = 'Bareboat Sailing Yachts';
		$v2_title = 'Crewed Sailing Yachts';

		$c1_title = 'Bareboat Catamarans';
		$c2_title = 'Crewed Catamarans';

		$bm1_title = 'Bareboat Motor Yachts';
		$bm2_title = 'Crewed Motor Yachts';
	}
	elseif($current_language_code=='es')
	{
		$v_title = 'Velero';
		$c_title = 'Catamarán';
		$bm_title = 'Barco a motor';

		$v1_title = 'Con o sin patrón';
		$v2_title = 'Con patrón o tripulacíon';

		$c1_title = 'Con o sin patrón';
		$c2_title = 'Con patrón o tripulacíon';

		$bm1_title = 'Con o sin patrón';
		$bm2_title = 'Con patrón o tripulacíon';
	}


	extract(shortcode_atts(array(
            'id'    => '10'
    ), $atts));



	$output .= '<div id="boats-feat" class="shortcode-voili">';
	$output .= '<div class="container">';
	$output .= '<div class="row">';


		$output .= '<article class="boat-item visible-animation col-lg-4 col-md-4 col-sm-12 col-xs-12">';
		$output .= '<div class="item item-7 cf">';
		$output .= '<div class="wrap cat-7">';
		$output .= '<div class="svg">'.$v_svg.'</div>';
		$output .= '<h4>'.$v_title.'</h4>';
		$output .= '<a class="boat-page-link" style="display: block;" href="'.$v_1.'">'.$v1_title.'</a>';
		$output .= '<a class="boat-page-link" style="display: block;" href="'.$v_2.'">'.$v2_title.'</a>';
		$output .= '</div>';
		$output .= '</div>';
		$output .= '</article>';

		$output .= '<article class="boat-item visible-animation col-lg-4 col-md-4 col-sm-12 col-xs-12">';
		$output .= '<div class="item item-8 cf">';
		$output .= '<div class="wrap cat-8">';
		$output .= '<div class="svg">'.$c_svg.'</div>';
		$output .= '<h4>'.$c_title.'</h4>';
		$output .= '<a class="boat-page-link" style="display: block" href="'.$c_1.'">'.$c1_title.'</a>';
		$output .= '<a class="boat-page-link" style="display: block;" href="'.$c_2.'">'.$c2_title.'</a>';
		$output .= '</div>';
		$output .= '</div>';
		$output .= '</article>';

		$output .= '<article class="boat-item visible-animation col-lg-4 col-md-4 col-sm-12 col-xs-12">';
		$output .= '<div class="item item-1 cf">';
		$output .= '<div class="wrap cat-1">';
		$output .= '<div class="svg">'.$bm_svg.'</div>';
		$output .= '<h4>'.$bm_title.'</h4>';
		$output .= '<a class="boat-page-link" style="display: block;" href="'.$bm_1.'">'.$bm1_title.'</a>';
		$output .= '<a class="boat-page-link" style="display: block;" href="'.$bm_2.'">'.$bm2_title.'</a>';
		$output .= '</div>';
		$output .= '</div>';
		$output .= '</article>';


	$output .= '</div>';
	$output .= '</div>';
	$output .= '</div>';


	return $output;
}


add_shortcode('boats_categories', 'boats_categories');


/*-------------------------------------------------------------------------------------------------------
		10. Shortcode: Yachts cpt
--------------------------------------------------------------------------------------------------------*/
function yachts_shortcode($atts)
{
	$output = '';

	//Options
    extract(shortcode_atts(array(
            'limit'    => '-1',
            'category'  => '-1'
            ), $atts));

    global $post;

    //Query Options
    $args = array(
    'posts_per_page' => $limit,
    'orderby' => 'post_date',
    'post_type' => 'yachts',
    'tax_query' => array(
        'relation' => 'OR',
        array(
            'taxonomy' => 'category',
            'field' => 'term_id',
            'terms' => array('1','7','8')
        ),
    ));

    //Query for projects
    $get_articles = NEW WP_Query($args);
	$blank_gif = get_stylesheet_directory_uri().'/images/blank.gif';

	//Wrapper
	$output .= '<div class="container">';
	$output .= '<div class="row voilivoilou-slideshow slideshow absolute-arrows arrows lazy-slides yachts-carousel">';

	//Loop
    while($get_articles->have_posts())
    {
        $get_articles->the_post();
        $img_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "medium");
		$feat_img = (!empty($img_src))? $img_src[0]: '';

        $output .= '<article class="item slide col-lg-4 col-md-4 col-sm-6 col-xs-6">';
        $output .= '<a class="item-wrap" href="'.get_permalink().'">';
        $output .= '<div class="top">';
        $output .= '<h4>'.get_the_title().'</h4>';
        $output .= '<div class="content">'.get_field('yacht_small_description').'</div>';
        $output .= '</div>';
        $output .= '<div class="bottom">';
        $output .= '<div class="img slide-lazyload-init bg-cover" style="background-image:url('.$blank_gif.');" data-original="'.$feat_img.'"></div>';
        $output .= '</div>';
        $output .= '</a>';
        $output .= '</article>';

    };

    $output .= '</div>';
    $output .= '</div>';

    //Important: Reset wp query
    wp_reset_query();

    return $output;
}

add_shortcode('yachts', 'yachts_shortcode');



/*----------------------------------------------------------
		Social media
----------------------------------------------------------*/
function socialmedia($atts) {
	$output = '';

	extract(shortcode_atts(array(
            'linkedin'    => '',
            'facebook'    => '',
            'instagram'    => '',
            'google'    => '',
            'youtube'    => ''
    ), $atts));

	$fb = get_field('facebook', 'option');
	$ins = get_field('instagram', 'option');
	$lin = get_field('linkedin', 'option');
	$gg = get_field('google', 'option');
	$yt = get_field('youtube', 'option');

	$output .= '<nav class="social-media-links grey-icons hul cf">';
	$output .= '<ul>';

	if(!empty($fb))
	{
		$output .= '<li><a href="'.$fb.'" class="fa fa-facebook-square" target="_blank" rel="nofollow"></a></li>';
	}

	if(!empty($ins))
	{
		$output .= '<li><a href="'.$ins.'" class="fa fa-instagram" target="_blank" rel="nofollow"></a></li>';
	}

	if(!empty($lin))
	{
		$output .= '<li><a href="'.$lin.'" class="fa fa-linkedin-square" target="_blank" rel="nofollow"></a></li>';
	}

	if(!empty($gg))
	{
		$output .= '<li><a href="'.$gg.'" class="fa fa-google-plus-square" target="_blank" rel="nofollow"></a></li>';
	}


	if(!empty($yt))
	{
		$output .= '<li><a href="'.$yt.'" class="fa fa-youtube-square" target="_blank" rel="nofollow"></a></li>';
	}

	$output .= '</ul>';
	$output .= '</nav>';

	return $output;
}


add_shortcode('socialmedia', 'socialmedia');






/*----------------------------------------------------------
		Social media
----------------------------------------------------------*/
function faq($atts, $content = null) {

	$output = '';

	extract(shortcode_atts(array(
            'title' => '',
    ), $atts));


	$output .= '<div class="faq-wrapper">';
	$output .= '<span class="faq-title"><p><strong>'.$title.'</strong></p></span>';


	$output .= '<div class="faq-content">'.$content.'</div>';



	$output .= '</div>';

	return $output;
}


add_shortcode('faq', 'faq');
