<?php
	
/*-------------------------------------------------------------------------------
	Post Label
-------------------------------------------------------------------------------*/
function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Blog';
    $submenu['edit.php'][5][0] = 'All Blog Posts';
    $submenu['edit.php'][10][0] = 'Add Post';
    echo '';
}

function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Blog';
    $labels->singular_name = 'Blog';
    $labels->add_new = 'Add Post';
    $labels->add_new_item = 'Add Post';
    $labels->edit_item = 'Edit Post';
    $labels->new_item = 'Post';
    $labels->view_item = 'View Blog Post';
    $labels->search_items = 'Search Posts';
    $labels->not_found = 'No Blog post found';
    $labels->not_found_in_trash = 'No Blog posts found in Trash';
    $labels->all_items = 'All Blog Posts';
    $labels->menu_name = 'Blog';
    $labels->name_admin_bar = 'Blog';
}
 
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );



/*-------------------------------------------------------------------------------
	Page Label
-------------------------------------------------------------------------------*/
function revcon_change_page_label() {
    global $menu;
    global $submenu;
    $menu[20][0] = 'Website';
    echo '';
}

function revcon_change_page_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['page']->labels;
    $labels->name = 'Sitemap';
    $labels->singular_name = 'Webpage';
    $labels->add_new = 'Add New Page';
    $labels->add_new_item = 'Add New Page';
    $labels->edit_item = 'Edit Page';
    $labels->new_item = 'Webpage';
    $labels->view_item = 'View Page';
    $labels->search_items = 'Search Pages';
    $labels->not_found = 'No pages found';
    $labels->not_found_in_trash = 'No pages found in Trash';
    $labels->all_items = 'All Pages';
    $labels->menu_name = 'Webpage';
    $labels->name_admin_bar = 'Webpage';
}
 
add_action( 'admin_menu', 'revcon_change_page_label' );
add_action( 'init', 'revcon_change_page_object' );


/*-------------------------------------------------------------------------------------------------------
		11. Theme Settings
--------------------------------------------------------------------------------------------------------*/
if( function_exists('acf_add_options_page') ) 
{
	
	acf_add_options_page(array(
		'page_title' 	=> 'VoiliVoilou Settings',
		'menu_title' 	=> 'VoiliVoilou',
		'menu_slug' 	=> 'theme-options',
		'capability' 	=> 'edit_posts',
		'parent_slug' 	=> '',
		'position'		=> false,
		'icon_url'		=> false,
		'redirect' 	=> false
	));
}
