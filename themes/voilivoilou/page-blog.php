<?php
/*
Template Name: Template - Blog
*/
?>

<?php get_header(); ?>

	<div id="content" >
		
		<div id="blog-posts" itemscope itemtype="http://schema.org/Blog" class="posts-wrapper cf">
			<div class="posts-row">
	        <?php 
		        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		        
		        $args = array(
					'post_type'=> 'post',
					'order'    => 'DESC',
					'post_status' => 'publish',
					'paged' => $paged
				);
				
				query_posts( $args );

		     ?>
	        
	    	<?php if( have_posts() ): ?>
			
			
			<?php
				while( have_posts() ): the_post(); 
				
				get_template_part('elements/post');
				
				endwhile; 	
			?>
			</div>
			
			
			<div id="blog-pagination" class="loadmore-deactivated">	
				<?php
				the_posts_pagination();
				?>
				<div class="loader"></div>
			</div>
			
			<?php endif; ?>
			
			<?php wp_reset_query(); ?>
		</div>
	</div><!-- /#content -->

<?php get_footer(); ?>