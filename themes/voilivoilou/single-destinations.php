<?php get_header(); ?>

<?php include('elements/banner.php'); ?>

<div id="content" class="page-<?php print $post->post_name; ?>">

		<?php
			include('elements/description.php');
			include('elements/description_h3.php');
			include('elements/description_page.php');
		?>


		<section class="voili-feat remove-padding">
		<?php
            //Slideshow top - maps
            include('elements/slideshow.php');
        ?>
		</section>
		<?php
            //blocks
            include('elements/blocks.php');
        ?>

		<?php
            //Flexible content
            include('elements/flexible-content.php');
        ?>

	<?php if (have_posts()): the_post(); ?>

	<?php else: ?>
		<p>Sorry, this page not longer exists.</p>
	<?php endif; ?>

</div><!--END #content -->

<?php get_footer(); ?>
