<?php get_header(); ?>

<div id="content" class="page-<?php print $post->post_name; ?>">
	<div id="blog-posts" itemscope itemtype="http://schema.org/Blog" class="posts-wrapper cf">

	<?php if( have_posts() ): the_post(); ?>
				
	<?php include('elements/post.php'); ?>
	
	</div>
	
	<?php else: ?>
		<p>Sorry, this page not longer exists.</p>
	<?php endif; ?>
	
</div><!--END #content -->

<?php get_footer(); ?>
