<?php 
$posttags = get_the_tags();
if ($posttags) {
	echo '<cite class="right tags"><i class="fa fa-tags"></i> Tags: ';
  foreach($posttags as $tag)
  {
    echo '<a class="tag" href="/tag/'.$tag->name.'">'.$tag->name.'</a>'; 
  }
  	echo "</cite>";
}	
?>