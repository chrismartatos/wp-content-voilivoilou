<?php

/*	ITINERARY block for destinations
----------------------------------------------------------------------------*/
$it_option = get_field('add_itinerary');

if($it_option == 'Yes'):

?>

<?php

$post_type = get_post_type();


$args = array(
    'post_type'      => $post_type,
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
 );


$parent = new WP_Query( $args );

if ( $parent->have_posts() ) : ?>

	<section class="voili-feat">

	<?php $d_title = get_field('itinerary_title'); ?>

	<h3 class="feat-title"><?php echo $d_title; ?></h3>
	<div class="container sub-block">

    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
    <?php
	    $it_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "large");
	    $blank_gif = get_template_directory_uri().'/images/blank.gif';
    ?>
			<article class="row">
				<div class="item cf">
					<div class="col-lg-12">
						<div class="wrap cf">
							<div class="d-left entry-content">
								<h5 style="font-weight: bold; font-size: 1.2rem; line-height: 1.3;"><?php the_title(); ?></h5>
								<?php the_content(); ?>
							</div>
							<div class="d-right">
								<a class="img bg-cover lazyload-init" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" style="background-image:url(<?php echo $blank_gif; ?>)" data-original="<?php echo $it_img[0]; ?>"></a>
							</div>
						</div>
					</div>
				</div>
			</article>
    <?php endwhile; ?>

    </div>
	</section>
<?php endif; wp_reset_query(); ?>

<?php endif; ?>






<?php
/*	ITINERARY block for destinations
----------------------------------------------------------------------------*/
$yct_option = get_field('yachts_blocks');

if($yct_option == 'Yes'):

?>

<?php

$get_cat = get_field('boats_categories');

$yacht_args = array(
    'post_type'      => 'yachts',
    'posts_per_page' => -1,
    'order'          => 'ASC',
    'orderby'        => 'menu_order',
    'tax_query' => array(
		'relation' => 'OR',
		array(
			'taxonomy' => 'category',
			'field'    => 'term_id',
			'terms'    => array($get_cat),
		)
	)
 );


$yacht_parent = new WP_Query( $yacht_args );

if ( $yacht_parent->have_posts() ) : ?>

	<section class="voili-feat bg-lgrey">

	<?php $d_title = get_field('yacht_title'); ?>


	<h3 class="feat-title"><?php echo $d_title; ?></h3>

	<div class="container sub-block yachts-blocks">

    <?php while ( $yacht_parent->have_posts() ) : $yacht_parent->the_post(); ?>
    <?php
	    $it_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "medium");
		$details = get_field('yacht_small_description');
		$blank_gif = get_template_directory_uri().'/images/blank.gif';
    ?>
			<article class="row">
				<div class="item cf">
				<div class="col-lg-12">
					<div class="wrap cf">
						<div class="d-left entry-content">
							<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<h4><?php the_title(); ?></h4>
							</a>
							<a class="wrap-content link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<p><?php echo $details; ?></p>
							</a>
						</div>
						<div class="d-right">
							<a class="img bg-cover lazyload-init" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" style="background-image:url(<?php echo $blank_gif; ?>)" data-original="<?php echo $it_img[0]; ?>"></a>
						</div>
					</div>
				</div>
				</div>
			</article>
    <?php endwhile; ?>

    </div>
	</section>
<?php endif; wp_reset_query(); ?>


<?php
	endif;
?>

