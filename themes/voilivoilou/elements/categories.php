<nav class="blog-categories">
<?php 
foreach(get_the_category() as $category)
{
    echo '<a href="'.get_category_link($category->cat_ID).'">'.$category->cat_name.'</a> ';
}     
?>
</nav>