<?php global $current_language_code; ?>

<?php $main_title = get_field('main_title'); ?>

<div class="page-header-bg-video" data-title="Vos Vacances. En Voilier. En Grèce. ">
	
  <h1><?php echo $main_title; ?></h1>
	
  <video id="page-header-bg-video" autoplay playsinline muted loop autostart="true" preload="auto" poster="https://voilivoilou-9ac0.kxcdn.com/wp-content/uploads/video-cover-1.jpg">
    <source src="https://voilivoilou-9ac0.kxcdn.com/wp-content/uploads/540-medium.mp4" type="video/mp4">
  </video>
  <p style="text-align: center;">
	  <a style="border: 2px solid white; display: inline-block;" href="https://www.youtube.com/watch?v=4JrJPosVW-c" class="btn white" target="_blank"><?php 
	  if( $current_language_code=='fr' ) echo  "voir la vidéo"; else echo "watch video";
  ?></a></p>
</div>


