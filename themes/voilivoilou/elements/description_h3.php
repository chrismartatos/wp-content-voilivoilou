<?php 
	$d = get_field('description_h3');	
	$d_width = get_field('description_width');	
	$d_padding = get_field('remove_padding_desc_h3');
	
		
	//Padding	
	if($d_padding == "Top")
	{
		$padding_option = " remove-padding-top";
		
	} 
	elseif($d_padding == "Bottom")
	{
		$padding_option = " remove-padding-bottom";
		
	} 
	elseif($d_padding == "Both")
	{
		$padding_option = " remove-padding";
		
	}
	
	if(get_field('description_h3')):
?>		
<section class="voili-feat <?php echo $padding_option; ?>">
	<h3 class="feat-title <?php if($remove_title=="Yes"){ echo " removed-title"; } ?> <?php echo " width-".$d_width; ?>"><?php echo $d; ?></h3>
</section>
<?php		
	endif;
?>
