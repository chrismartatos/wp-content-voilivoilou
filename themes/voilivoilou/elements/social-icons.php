<nav class="social-media-links white-icons hul cf">
	<?php 
		$fb = get_field('facebook', 'option'); 
		$ins = get_field('instagram', 'option'); 
		$li = get_field('linkedin', 'option'); 
		$gg = get_field('google', 'option'); 
		$yt = get_field('youtube', 'option');
		$cl = get_field('catalogue', 'option');
	?>
	<ul>
		<li><a href="<?php echo $fb; ?>" class="fa fa-facebook-square" target="_blank" rel="nofollow"></a></li>
		<li><a href="<?php echo $ins; ?>" class="fa fa-instagram" target="_blank" rel="nofollow"></a></li>
		<li><a href="<?php echo $li; ?>" class="fa fa-linkedin-square" target="_blank" rel="nofollow"></a></li>
		<li><a href="<?php echo $gg; ?>" class="fa fa-google-plus-square" target="_blank" rel="nofollow"></a></li>
		<li><a href="<?php echo $yt; ?>" class="fa fa-youtube-square" target="_blank" rel="nofollow"></a></li>
		<li><a href="<?php echo $cl; ?>" class="fa fa-anchor" target="_blank" onclick="ga('send', 'event', 'clics', 'PDF - Brochure');" title="Téléchargez la brochure VoiliVoilou ⛵"></a></li>
	</ul>
</nav>