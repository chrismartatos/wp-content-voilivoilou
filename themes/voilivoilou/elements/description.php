<?php 
	$d = get_field('description');	
	$d_width = get_field('description_width');	
	$d_padding = get_field('remove_padding_desc');
	
		
	//Padding	
	if($d_padding == "Top")
	{
		$padding_option = " remove-padding-top";
		
	} 
	elseif($d_padding == "Bottom")
	{
		$padding_option = " remove-padding-bottom";
		
	} 
	elseif($d_padding == "Both")
	{
		$padding_option = " remove-padding";
		
	}
	
	if(get_field('description')):
?>		
<section class="voili-feat <?php echo $padding_option; ?>">
	<h2 class="desc <?php if($remove_title=="Yes"){ echo " removed-title"; } ?> <?php echo " width-".$d_width; ?>"><?php echo $d; ?></h2>
</section>
<?php		
	endif;
?>
