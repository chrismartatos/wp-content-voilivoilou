<nav class="social">
	<i class="fa fa-share-alt"></i> <a class="anchor-hover" href="#">Share +</a>
	<ul class="share-social">
		<li><a target="_blank" href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>" class="fa fa-facebook social-fb"></a></li>
		<li><a target="_blank" href="http://twitter.com/share?text=<?php ?>&amp;url=<?php the_permalink(); ?>" class="fa fa-twitter social-tw"></a></li>
		<li><a target="_blank" href="mailto:example@email.com?subject=<?php the_title(); ?>&amp;body=<?php the_permalink(); ?>" class="fa fa-envelope social-em"></a></li>
	</ul>
</nav>