<?php
//if( get_row_layout() == 'slideshow' ):
if(get_field('destinations_slideshow') == 'Yes'):
?> 
    	<div class="container-fluid">
        	<div class="row">
				<div id="main-content-slideshow" class="slideshow-fx slideshow absolute-arrows absolute-dots arrows bullets titles">
		
					<?php while( have_rows('slideshow') ): the_row(); 

					// vars
					$content_left = get_sub_field('image');
					$content_option = get_sub_field('add_content');
					$content_right = get_sub_field('slide_content');
					$t_top = get_sub_field('title_top');
					$t_bottom = get_sub_field('title_bottom');
					$css_image = get_sub_field('slide_background_image');
					$blank_img = get_stylesheet_directory_uri().'/images/blank.gif';
					$map = "img-map";
					
					?>
			
						<?php if( $content_option == "Yes" ): ?>
							
								<div class="slide item cf slide-2col">
									<div class="vcenter-outer">
										<div class="vcenter-inner">
											<div class="container">
											<div class="row">
												<?php if(!empty($t_top)){ echo '<h3 class="feat-title title-top">'.$t_top.'</h3>'; } ?>
											</div>
											<div class="row">
												<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
													<div class="entry-content">
													<?php echo add_lazyload($content_left); ?>
													</div>
												</div>
												<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
													<div class="entry-content">
													<?php echo add_lazyload($content_right); ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php if(!empty($t_bottom)){ echo '<h4 class="title-bottom">'.$t_bottom.'</h4>'; } ?>
				            </div>
						<?php 
							else:
							
						?>
						
							<div class="cf slide item slide-1col bg-active <?php echo $content_option.'-'.$map; ?>">
				                <img alt="<?php echo the_title(); ?>" src="<?php echo $css_image['url']; ?>">
				                <?php if(!empty($t_bottom)){ echo '<h4 class="title-bottom">'.$t_bottom.'</h4>'; } ?>
				            </div>
						<?php endif; ?>
									
					<?php endwhile; ?>
					
					<nav class="dots-nav">
						
						<div class="wrap">
							<div class="append-dots"></div>
							<div class="append-arrows"></div>
						</div>
					</nav>
							    
				</div>
        	</div>
    	</div>
<?php
endif;
?>