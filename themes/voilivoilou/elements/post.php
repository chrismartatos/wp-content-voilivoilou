<?php 
	$postid = get_the_ID();
    $thumb_id = get_post_thumbnail_id();
	$full_image = wp_get_attachment_image_src($thumb_id,'large', false);
	$med_image = wp_get_attachment_image_src($thumb_id,'medium', false);
	$share = ($current_language_code=="fr")? 'Partager': 'Share';
?>

<article itemscope itemtype="http://schema.org/blogPost" id="post-<?php the_ID(); ?>" <?php post_class('post generic-layout'); ?>>
	<div class="container">
		<div class="row">
			
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12"></div>
			
			<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
				
				<!--Date-->
				<time class="date" itemprop="dateCreated"><?php the_time('l d.m.Y'); ?></time>
	    		
	    		<!--Share-->
	    		<nav class="share hul">		    		
		    		<div class="hover"><?php echo $share; ?> <span class="fa fa-share-alt"></span></hover>
		    		
		    		<ul class="share-buttons hul">
			    		<li><a class="fa fa-facebook social-fb" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" target="_blank"></a></li>
			    		<li><a class="fa fa-twitter social-tw" href="http://twitter.com/share?text=&amp;url=<?php the_permalink(); ?>" target="_blank"></a></li>
			    		<li><a class="fa fa-envelope social-em" href="mailto:example@email.com?subject=<?php the_title(); ?>&amp;body=<?php the_permalink(); ?>" target="_blank"></a></li>
			    	</ul>
	    		</nav>
	    		
	    		<!--Categories-->
	    		<nav class="blog-categories hide">
			    	<?php 
					foreach(get_the_category() as $category)
					{
					    echo '<a href="'.get_category_link($category->cat_ID).'">'.$category->cat_name.'</a> ';
					}     
				    ?>
				</nav>
			</div>
			
			<div class="col-md-7 col-lg-7 col-sm-9 col-xs-12">
				<a class="title" href="<?php the_permalink(); ?>">
					<h2 class="post-title"><?php the_title(); ?></h2>
				</a>
				<div class="entry-content">
					
					<?php if(!empty($full_image)): ?>
					<p><img src="<?php echo $full_image[0]; ?>" alt="<?php the_title(); ?>"></p>
					<?php endif; ?>
					
					<?php the_content(); ?>
				</div>
			</div>
			
		</div>
	</div>
</article>

