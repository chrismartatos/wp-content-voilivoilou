<?php 
$imgsrc = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full");
$getimg = $imgsrc[0];

$get_banner = get_field('banner');
$remove_title = get_field('remove_title');
$title_size = get_field('title_size');

?>


<div id="intro-header" class="cf<?php if($remove_title=="Yes"){ echo " remove-title"; } ?><?php echo " size".$title_size; ?>">

<?php if(!empty($get_banner)): ?>
<div id="hero-banner" class="banner bg-cover" <?php echo 'style="background-image:url('.$get_banner.');"'; ?> data-scrollax-parent="true">
	<div class="vcenter-outer">	
		<div class="vcenter-inner">
			<h1 data-scrollax="properties: { 'translateY': '240%'}"><?php the_title(); ?></h1>
		</div>
	</div>
</div>


<?php elseif(!empty($getimg)): ?>
<div id="hero-banner" class="banner bg-cover" <?php echo 'style="background-image:url('.$getimg.');"'; ?> data-scrollax-parent="true">
	<div class="vcenter-outer">	
		<div class="vcenter-inner">
			<h1 data-scrollax="properties: { 'translateY': '240%'}"><?php the_title(); ?></h1>
		</div>
	</div>
</div>

<?php else: ?>

	<h1 class="page-title"><?php the_title(); ?></h1>
	
<?php endif; ?>

</div>

