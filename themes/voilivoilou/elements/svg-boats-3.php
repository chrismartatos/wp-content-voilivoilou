<svg version="1.1" id="boat-motor" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="160px" height="160px" viewBox="0 0 160 160" enable-background="new 0 0 160 160" xml:space="preserve">
<g>
	<path fill="none" stroke="#58595B" stroke-width="2.6723" stroke-miterlimit="10" d="M-293.424,127.512
		c17.5,0,34.359-21.221,34.359-47.393c0-26.194-20.36-47.415-34.359-47.415V127.512z"/>
</g>
<path fill="none" stroke="#58595B" stroke-width="2.6723" stroke-miterlimit="10" d="M-144.403,40.749
	c0,17.5,21.221,34.359,47.393,34.359c26.193,0,47.414-20.363,47.414-34.359H-144.403z"/>
<path fill="none" stroke="#58595B" stroke-width="2.6723" stroke-miterlimit="10" d="M-144.403,86.561
	c0,17.5,21.221,34.359,47.393,34.359c26.193,0,47.414-20.363,47.414-34.359H-144.403z"/>
<g>
	<path fill="none" stroke="#58595B" stroke-width="2.6723" stroke-miterlimit="10" d="M80,79.53
		c14.354,0,28.185-17.411,28.185-38.877c0-21.481-16.702-38.892-28.185-38.892V79.53z"/>
	<path fill="none" stroke="#58595B" stroke-width="2.6723" stroke-miterlimit="10" d="M80.008,80.835
		c-14.361,0-28.186,17.402-28.186,38.877c0,21.481,16.702,38.892,28.186,38.892V80.835z"/>
	<path fill="none" stroke="#58595B" stroke-width="2.6723" stroke-miterlimit="10" d="M79.352,80.186
		c0-14.353-17.411-28.185-38.878-28.185c-21.489,0-38.892,16.695-38.892,28.185H79.352z"/>
	<path fill="none" stroke="#58595B" stroke-width="2.6723" stroke-miterlimit="10" d="M80.648,80.179
		c0,14.353,17.41,28.185,38.877,28.185c21.489,0,38.893-16.702,38.893-28.185H80.648z"/>
</g>
</svg>
