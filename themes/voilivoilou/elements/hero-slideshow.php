<div id="hero-slideshow" class="cf" data-scrollax-parent="true">
	
	<?php $main_title = get_field('main_title'); ?>
	
	<div data-scrollax="properties: { 'translateY': '90%'}">
	
		<h1><?php echo $main_title; ?></h1>
	
		<div class="slideshow arrows absolute-arrows bullets slick-slider">
			
		<?php if( have_rows('slideshow') ): ?>
		
		<?php while( have_rows('slideshow') ): the_row(); 
	
			// vars
			$image = get_sub_field('slide_image');
			$content = get_sub_field('slide_description');
			$title = get_sub_field('slide_title');
			$link = get_sub_field('link');
			$blank = get_stylesheet_directory_uri().'/images/blank.gif';
			?>
			<article class="slick-slide slide-wrap" onclick="location.href='<?php echo $link; ?>'">
				<figure class="slide-img bg-cover" style="background-image:url(<?php echo $image['url']; ?>)" data-original="<?php echo $image['url']; ?>">
				</figure>
				
				<?php if( $link ): ?>
				<a class="link" href="<?php echo $link; ?>">
				<?php endif; ?>
				
					<h2><?php echo $title; ?></h2>
				
				<?php if( $link ): ?>
				</a>
				<?php endif; ?>
				
			    <p>
				    <?php if( $link ): ?>
					<a class="link" href="<?php echo $link; ?>">
					<?php endif; ?>
				    <?php echo $content; ?>
				    <?php if( $link ): ?>
					</a>
					<?php endif; ?>
				</p>
			</article>
		<?php endwhile; ?>
		
		<?php endif; ?>
		</div>
	</div>
</div>