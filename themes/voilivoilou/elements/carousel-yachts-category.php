<?php
    $get_yachts_cat = get_field('choose_yachts_categories');
    
    
    if( empty($get_yachts_cat) || $get_yachts_cat == "" ):
    
    	$args = array(
	    'posts_per_page' => '-1', 
	    'orderby' => 'menu_order',
	    'post_type' => 'yachts',
	    'tax_query' => array(
	        'relation' => 'OR',
	        array(
	            'taxonomy' => 'category',
	            'field' => 'term_id',
	            'terms' => array('8','1','7')
	        ),
	    ));
	    
	    else:
	    
		$args = array(
	    'posts_per_page' => '-1', 
	    'orderby' => 'menu_order',
	    'post_type' => 'yachts',
	    'tax_query' => array(
	        'relation' => 'OR',
	        array(
	            'taxonomy' => 'category',
	            'field' => 'term_id',
	            'terms' => array($get_yachts_cat)
	        ),
	    ));
	    
    endif;
    
    
	
	
    
    $query = NEW WP_Query($args);
    
	$blank_gif = get_stylesheet_directory_uri().'/images/blank.gif';
	
		
	  if ( $query->have_posts() )
      {
	      echo '<div class="container">';
	      echo '<div class="row voilivoilou-slideshow slideshow absolute-arrows arrows lazy-slides yachts-carousel choose-yachts-carousel">';
	      
            while ( $query->have_posts() ) : $query->the_post();
			
			$img_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "medium");
			$feat_img = $img_src[0];
		
            echo '<article class="item slide col-lg-4 col-md-4 col-sm-6 col-xs-6">
			         <a class="item-wrap" href="'.get_permalink().'">
			         <div class="top">
			         <h4>'.get_the_title().'</h4 >
			         <div class="content">'.get_field('yacht_small_description').'</div>
			         </div>
			         <div class="bottom" >
			         <div class="img slide-lazyload-init bg-cover" style="background-image:url('.$blank_gif.');" data-original="'.$feat_img.'"></div>
			         </div>
			         </a>
			     </article>';

            endwhile;
			
			echo '</div>';
			echo '</div>';
			
            wp_reset_postdata();
          }
?>

    
    
    
    
    


