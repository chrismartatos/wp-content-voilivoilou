<svg version="1.1" id="voilivoilou-svg-logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="160px" height="160px" viewBox="0 0 160 160" enable-background="new 0 0 160 160" xml:space="preserve">
<g>
	<g>
		<path fill="none" stroke="#0050A4" stroke-width="3.1351" stroke-miterlimit="10" d="M87.806,39.725
			c0-3.609-2.929-6.54-6.546-6.54c-3.606,0-6.538,2.931-6.538,6.54c0,3.608,2.931,6.54,6.538,6.54
			C84.877,46.265,87.806,43.333,87.806,39.725z"/>
		<polyline fill="none" stroke="#0050A4" stroke-width="3.1351" stroke-linecap="round" stroke-miterlimit="10" points="
			104.886,79.882 81.26,105.9 57.644,79.882 		"/>
		<path fill="none" stroke="#0050A4" stroke-width="3.1351" stroke-miterlimit="10" d="M81.26,57.164"/>
		<path fill="none" stroke="#0050A4" stroke-width="3.1351" stroke-miterlimit="10" d="M81.26,91.3"/>
		<polyline fill="none" stroke="#0050A4" stroke-width="3.1351" stroke-linecap="round" stroke-miterlimit="10" points="
			111.231,92.5 81.26,124.473 51.283,92.5 		"/>
		
			<line fill="none" stroke="#0050A4" stroke-width="3.1351" stroke-linecap="round" stroke-miterlimit="10" x1="70.412" y1="58.979" x2="92.111" y2="58.979"/>
	</g>
	<path fill="none" stroke="#0050A4" stroke-width="3.1351" stroke-miterlimit="10" d="M157.11,80.168
		c0-42.645-34.561-77.205-77.205-77.205c-42.638,0-77.204,34.56-77.204,77.205c0,42.638,34.566,77.202,77.204,77.202
		C122.55,157.37,157.11,122.806,157.11,80.168z"/>
</g>
</svg>