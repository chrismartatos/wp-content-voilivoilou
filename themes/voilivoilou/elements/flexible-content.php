<?php 
	$fx_padding = get_field('remove_padding_fx');
	
		
	//Padding	
	if($fx_padding == "Top")
	{
		$padding_option_fx = " remove-padding-top";
		
	} 
	elseif($fx_padding == "Bottom")
	{
		$padding_option_fx = " remove-padding-bottom";
		
	} 
	elseif($fx_padding == "Both")
	{
		$padding_option_fx = " remove-padding";
		
	}
?>

<section id="flexible-content-wrapper" class="voili-feat<?php echo $padding_option_fx; ?>">	
<div id="flexible-content" class="fx-wrapper cf">
<?php
	
	// check if the flexible content field has rows of data
	if( have_rows('add_content') ):
	
	     // loop through the rows of data
	    while ( have_rows('add_content') ) : the_row();
		
			if( get_row_layout() == 'shortcodes' ):
	
	        	$shortcodes = get_sub_field('shortcodes');
	        	$row_padding = get_sub_field('remove_padding');
				$padding_option_row = "";
		
				//Padding	
				if($row_padding == "Top")
				{
					$padding_option_row = " remove-padding-top";
					
				} 
				elseif($row_padding == "Bottom")
				{
					$padding_option_row = " remove-padding-bottom";
					
				} 
				elseif($row_padding == "Both")
				{
					$padding_option_row = " remove-padding";
					
				}
			?>
			
				<div class="fx-shortcodes">
					<?php echo $shortcodes; ?>
				</div>
			
			<?php
	        elseif( get_row_layout() == '6_columns_center' ):
	
	        	$c6 = get_sub_field('content');
			?>
			
			<article class="entry-content six-col-c">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-lg-3 col-sm-1 col-xs-12 grid-item"> </div>
					<div class="col-md-6 col-lg-6 col-sm-10 col-xs-12 grid-item"><?php echo $c6; ?></div>
					<div class="col-md-3 col-lg-3 col-sm-1 col-xs-12 grid-item"> </div>
				</div>
			</div>
			</article>
			
			<?php
	        elseif( get_row_layout() == '4_columns_center' ):
	
	        	$c4 = get_sub_field('content');
			?>
			<article class="entry-content four-col-c">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-lg-4 col-sm-2 col-xs-1 grid-item"> </div>
					<div class="col-md-4 col-lg-4 col-sm-8 col-xs-12 grid-item"><?php echo $c4; ?></div>
					<div class="col-md-5 col-lg-4 col-sm-2 col-xs-12 grid-item"> </div>
				</div>
			</div>
			</article>
			<?php
	        elseif( get_row_layout() == '2_columns_center' ):
	
	        	$c2 = get_sub_field('content');
			?>
			<article class="entry-content two-col-c">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-lg-5 col-sm-4 col-xs-12 grid-item"> </div>
					<div class="col-md-2 col-lg-2 col-sm-4 col-xs-12 grid-item"><?php echo $c2; ?></div>
					<div class="col-md-5 col-lg-5 col-sm-4 col-xs-12 grid-item"> </div>
				</div>
			</div>
			</article>
			
			<?php
	        elseif( get_row_layout() == '3_columns_center' ):
	
	        	$c2 = get_sub_field('content');
			?>
			<article class="entry-content two-col-c">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-lg-5 col-sm-4 col-xs-12 grid-item"> </div>
					<div class="col-md-3 col-lg-3 col-sm-4 col-xs-12 grid-item"><?php echo $c2; ?></div>
					<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 grid-item"> </div>
				</div>
			</div>
			</article>
			
			<?php
	        elseif( get_row_layout() == 'generic_layout' ):
	
	        	$c_t = get_sub_field('title');
	        	$c_c = get_sub_field('content');
			?>
			<div class="generic-layout">
				<div class="container">
					<div class="row">
						<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"></div>
						<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 grid-item">
							<h2 class="left-title">
								<?php 
									if(get_sub_field('title'))
									{
										echo $c_t;
									} else {
										the_title();
									}
								?>
							</h2>
						</div>
						<div class="col-md-7 col-lg-7 col-sm-7 col-xs-12 grid-item">
							<div class="entry-content right-content">
							<?php echo $c_c; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<?php
			/*		Faq
			----------------------------------------------------------*/
	        elseif( get_row_layout() == 'generic_layout_faq' ):
	
	        	$faq_t = get_sub_field('title');
	        	$faq_section_title = get_sub_field('faq');
			?>
			<div class="generic-layout">
				<div class="container">
					<div class="row">
						<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"></div>
						<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 grid-item">
							<h2 class="left-title">
								<?php 
									echo $faq_t;
								?>
							</h2>
						</div>
						
						<div class="col-md-7 col-lg-7 col-sm-7 col-xs-12 grid-item">
							<div class="entry-content right-content faq-section">
							<?php if($faq_section_title): ?>
							<h3 class="title"><?php echo $faq_section_title; ?></h3>	
							<?php endif; ?>
							<?php 							
							if( have_rows('content_faq') ):
							
							    while ( have_rows('content_faq') ) : the_row();
							
							        // display a sub field value
							        $faq_title = get_sub_field('title_faq');
							        $faq_content = get_sub_field('content_faq');
									?>
									
									<div class="faq-wrapper">
										<div class="faq-title">
											<p><strong><?php echo $faq_title; ?></strong></p>
										</div>
										<div class="faq-content">
											<?php echo  $faq_content; ?>
										</div>
									</div>
									
									<?php
							    endwhile;
							
							endif;	
							?>	
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<?php
	        elseif( get_row_layout() == '6_col_6_col' ):
	
	        	$c_l = get_sub_field('content_left');
	        	$c_r = get_sub_field('content_right');
	        	
	        	$ap = get_sub_field('add_padding');
	        	$bg = get_sub_field('background_color');
	        	$bg_img = get_sub_field('background_image');
			?>
			<div class="bg-fixed bg-cover cf<?php if($ap=="Yes"){ echo " voili-feat"; } ?><?php if($bg=="White"){ echo " bg-white"; }elseif($bg=="Grey"){ echo " bg-grey"; }elseif($bg=="LightGrey"){ echo " bg-lgrey"; } ?>"<?php if($bg_img['url']){ echo ' style="background-image:url('.$bg_img['url'].');"'; } ?>>
			<div class="container half-col-fx">
				<div class="row">
					<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 grid-item"><?php echo $c_l; ?></div>
					<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 grid-item"><?php echo $c_r; ?></div>
				</div>
			</div>
			</div>
			
			<?php
	        elseif( get_row_layout() == '3_col_3_col_3_col' ):
	
				$c_t = get_sub_field('title');
	        	$c_l_1 = get_sub_field('content_col_3_1');
	        	$c_l_2 = get_sub_field('content_col_3_2');
	        	$c_l_3 = get_sub_field('content_col_3_3');
			?>
			<div class="container entry-content">
				<div class="row">
					<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 grid-item"><?php echo $c_l_1; ?></div>
					<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 grid-item"><?php echo $c_l_2; ?></div>
					<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 grid-item"><?php echo $c_l_3; ?></div>
				</div>
			</div>
			
			
			
			<?php
	        elseif( get_row_layout() == 'content_editor' ):
	
	        	$c_t = get_sub_field('title');
	        	$c_c = get_sub_field('content');
	        	
	        	$bg = get_sub_field('background_color');
	        	$row_padding = get_sub_field('remove_padding');
				$padding_option_row = "";
		
				//Padding	
				if($row_padding == "Top")
				{
					$padding_option_row = " remove-padding-top";
					
				} 
				elseif($row_padding == "Bottom")
				{
					$padding_option_row = " remove-padding-bottom";
					
				} 
				elseif($row_padding == "Both")
				{
					$padding_option_row = " remove-padding";
					
				}
			?>
			
			<section class="voili-feat<?php echo $padding_option_row; ?><?php if($bg=="White"){ echo " bg-white"; }elseif($bg=="Grey"){ echo " bg-grey"; }elseif($bg=="LightGrey"){ echo " bg-lgrey"; } ?>">
				<?php if(!empty($c_t)){ echo '<h3 class="feat-title">'.$c_t.'</h3>'; } ?>
				<div class="text-editor entry-content">
				<?php echo $c_c; ?>
				</div>
			</section>
			
			
			<?php
			//CAROUSEL 2 images
	        elseif( get_row_layout() == 'carousel' ):
	
	        	$c_t = get_sub_field('title');
	        	$bg = get_sub_field('background_color');
	        ?>
	        <div class="cf carousel-fx voili-feat <?php if($bg=="White"){ echo " bg-white"; }elseif($bg=="Grey"){ echo " bg-grey"; }elseif($bg=="LightGrey"){ echo " bg-lgrey"; } ?>">
		        <?php if($c_t): ?>
		        	<h3 class="feat-title"><?php echo $c_t; ?></h3>
		        <?php endif; ?>
		        <div class="container">
			        <div class="row">
				        <div class="cf slideshow carousel-two-slides absolute-arrows arrows">
				        <?php	
				        	if( have_rows('carousel_images') ):
			
							 	// loop through the rows of data
							    while ( have_rows('carousel_images') ) : the_row();
							    
							    $carousel_t = get_sub_field('title');
							    $carousel_img = get_sub_field('image');
							    $carousel_desc = get_sub_field('description');
							    ?>
								
									<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 slide-item">
										<div class="row">
											<h4 class="title"><?php echo $carousel_t; ?></h4>
											
											<?php if($carousel_desc): ?>
												
												<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12"><img class="img" src="<?php echo $carousel_img['url']; ?>" alt="<?php echo $carousel_t; ?>"></div>
												<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
													<div class="desc">
													<?php echo $carousel_desc; ?>
													</div>
												</div>
											
												<?php else: ?>
												
												<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12"><img class="centered" src="<?php echo $carousel_img['url']; ?>" alt="<?php echo $carousel_t; ?>"></div>
											<?php endif; ?>
											
											
										</div>
									</div>
								
								<?php
							    endwhile;
								
							endif;
						?>
				        </div>
					</div>
				</div>
	        </div>
			
			
				
			
			
			
			<?php
			//END flexible contnet
	        endif;
	
	    endwhile;
	
	endif;
	?>
</div>
</section>