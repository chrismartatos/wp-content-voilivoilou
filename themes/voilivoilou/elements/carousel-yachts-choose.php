<?php
    global $blank_gif;

    $slideshow_post_object = get_field('choose_yachts_to_show');

    if ($slideshow_post_object) {
        echo '<div class="container">';
        echo '<div class="row voilivoilou-slideshow slideshow absolute-arrows arrows lazy-slides yachts-carousel choose-yachts-carousel">';

        foreach ($slideshow_post_object as $post) {
            setup_postdata($post);

            $img_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), "medium");
            $feat_img = $img_src[0];

            echo '<article class="item slide col-lg-4 col-md-4 col-sm-6 col-xs-6">
		         <a class="item-wrap" href="'.get_permalink().'">
		         <div class="top">
		         <h4>'.get_the_title().'</h4 >
		         <div class="content">'.get_field('yacht_small_description').'</div>
		         </div>
		         <div class="bottom">
		         <div class="img slide-lazyload-init bg-cover" style="background-image:url('.$blank_gif.');" data-original="'.$feat_img.'"></div>
		         </div>
		         </a>
		     </article>';
        }

        echo '</div>';
        echo '</div>';

        wp_reset_postdata();
    }

?>








