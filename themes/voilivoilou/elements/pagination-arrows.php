<?php
if(is_single()):
	global $current_language_code;
	global $post; 
	global $post_type;
	
	
	//Prev next
	$prevPost = get_previous_post();
	$nextPost = get_next_post();
	$current_lang = "";
	$back_href = "";
      
	//Back button
	if($post_type=="destinations"):
		
		$back_href = "/destinations/";
		
	elseif($post_type=="yachts"):
		
		$term = get_the_terms( get_the_ID(), 'category');
		
		$current_lang = ( $current_language_code=='fr' ) ? "/bateaux/" : "/yachts/" ;
		
		//$back_href = $term[0]->slug;
		$back_href = "";
	
		$active_cat_id = $term[0]->term_id;
		
		$prevPost = get_previous_post($active_cat_id);
		$nextPost = get_next_post($active_cat_id);
	
	endif;
?>
<nav id="work-nav" class="cf">
	<?php 
	if(!empty($prevPost)):
	
	$prevTitle = get_the_title($prevPost->ID);
	$prevLink = get_permalink($prevPost->ID);
    $prevThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($prevPost->ID,'thumbnail'));
	?>
    <a class="prev arrows" rel="prev" href="<?php echo $prevLink; ?>" title="<?php echo $prevTitle; ?>" title="<?php echo $prevTitle; ?>">
        <span class="ent-arr">&larr;</span>
        <div class="thumb bg-cover" style="background-image:url(<?php echo $prevThumbnail[0]; ?>);"></div>
    </a>	
    <?php endif; ?>
    
    
	<a class="all" href="<?php echo get_home_url(); ?><?php echo $current_lang; ?><?php echo $back_href; ?>" data-lang="<?php echo $current_lang; ?>">
		<div class="wrap cf">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</a>
	
	<?php 
	if(!empty($nextPost)): 
	
	$nextTitle = get_the_title($nextPost->ID);
	$nextLink = get_permalink($nextPost->ID);        
    $nextThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($nextPost->ID,'thumbnail'));
	?>
	<a class="next arrows" rel="next" href="<?php echo $nextLink; ?>" title="<?php echo $nextTitle; ?>" data-title="<?php echo $nextTitle; ?>">
        <span class="ent-arr">&rarr;</span> 
        <div class="thumb bg-cover" style="background-image:url(<?php echo $nextThumbnail[0]; ?>);"></div>
    </a>
    <?php endif; ?>
</nav>
<?php endif; ?>