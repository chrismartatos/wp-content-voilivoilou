<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri (); ?>/admin/admin.css">

<section class="admin-doc cf">
<h1>Documentation</h1>

<nav class="admin-doc-nav cf">
	<ul>
		<li><a href="#general">General</a></li>
		<li><span class="divider">|</span></li>
		<li><a href="#layout">Layout Shortcodes</a></li>
		<li><span class="divider">|</span></li>
		<li><a href="#media">Media Shortcodes</a></li>
	</ul>
</nav>

<section class="admin-doc-section" id="general">
<h2>General</h2>

<div class="wrap cf">
	<h3>Page Templates</h3>
	<p>There are serveral templates in this Theme. You should be using the Default Template for all sub pages. All other templates such as Homepage, Programs, Institutes should not be changed on the client side.</p>
</div>
</section>

<section class="admin-doc-section" id="layout">
<h2>Layout Shortcodes</h2>

<div class="wrap cf">
<h3>Example</h3>
<div class="code">
<pre>
[example] *content* [/example]
</pre>
</div>

</div>
</section>

<section class="admin-doc-section" id="media">
<h2>Media Shortcodes</h2>
<div class="wrap cf">
<h3>Image Lightbox</h3>
<div class="code">
<pre>
[lighbox] *select an image from media* [/lightbox]
</pre>
</div>

<h3>Photo Gallery</h3>
<div class="code">
<pre>
*create a photo gallery from media*
</pre>
</div>

</div>
</section>

</section>