<?php
/*
Template Name: Template - Boats Crewed 
*/
?>

<?php get_header(); ?>
	
<?php include('elements/banner.php'); ?>

<div id="content" class="page-<?php print $post->post_name; ?>">
	<?php if( have_posts() ): the_post(); ?>
	
	<section class="voili-feat bg-white remove-padding-bottom">
		
		<?php include('elements/description.php'); ?>
		
		<?php
			//Flexible content 
			include('elements/flexible-content.php'); 
		?>
		
		
		<?php 
			//Itinerary
			get_template_part('elements/blocks-crewed');
		?>
		
		<div class="voili-feat bg-lgrey remove-padding-top">
			<article class="text-editor entry-content">
				<?php the_content(); ?>
			</article>
		</div>

		
	</section>
	
	
	<?php else: ?>
		<p>Sorry, this page not longer exists.</p>
	<?php endif; ?>
	
</div><!--END #content -->

<?php get_footer(); ?>
