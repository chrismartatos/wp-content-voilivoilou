(function($) 
{
  'use strict';
  
  /* Init */
  var voilivoilou = window.voilivoilou || {};
  
  /* FN: onload basics 
  --------------------------------------------------------------------------------------------------------------------*/
  voilivoilou.onLoadBasics = function()
  {
  	 var events = {
	  	mobileNav: function()
	  	{
		  	//Hamburger
		  	$("#hamburger").click(function(e)
			{
				e.preventDefault();
				
				var $this = $(this),
					$menu = $("#mobile-fixed-nav"),
					$body = $("body");
			
				
				$this.toggleClass("active");
				$body.toggleClass("mobile-menu-open");
				$menu.toggleClass("opened");
				
				return false;
			});
	  	},
	  	mainNav: function()
	  	{
		  var $window = $(window), 
			  $pos = $window.scrollTop(), 
		      $header = $("#header"),
		      $height = $window.height(),
	  		  _height = 2 / 3,
	  		  _multiply = _height * $height,
	  		  _2_3rds = _multiply / 3,
	  		  _result = $window.height() - _2_3rds;
		      
		  //Sticky header
		  if($("body.home").length)
		  {
		      $nav_pos = _result;
	          
	          //Resize 
	          $window.resize(function()
		  	  {
			  	// Get:2/3
			  	$height = $window.height(),
	  		    _height = 2 / 3,
	  		    _multiply = _height * $height,
	  		    _2_3rds = _multiply / 3,
	  		    _result = $window.height() - _2_3rds,  
			  	$nav_pos = _result;
			  	
			  	$header.css("top", $nav_pos);
			  	
			  	return $nav_pos;
		  	  });
	          
	          //Add top height
	          $header.css("top", "68vh");
	          
	          //Fixed header on scroll
		      $window.scroll(function()
		      {
		         $pos = $window.scrollTop(); 
		         ($pos>=$nav_pos)?$header.addClass("fixed-header"):$header.removeClass("fixed-header");
		      }); 
		  } 
		  else 
		  {
			  //prepare vars for scrolling effect
			  var $intro = $("#intro-header"),
			  	  $gutter = $intro.height()-110,
				  $nav_pos = $gutter;
			  	  
			  	  //Change header bg color
			      $window.scroll(function()
			      {
			         $pos = $window.scrollTop(); 
			         ($pos>=$nav_pos)?$header.addClass("add-bg"):$header.removeClass("add-bg");
			      });
			  
			  if($("#hero-banner").length)
			  {
				 //Class for nav colors
			  	 $header.addClass("template-banner");
			  }
		  }
		  
		  $("#main-nav ul > li.menu-item-has-children").hoverIntent({    
			    sensitivity: 1, 
			    interval: 10,     
			    timeout: 100,   
			    over:function(){
			        $(".sub-menu:first",this)
			            .removeClass("hoverOut").toggleClass("hoverIn").fadeIn();
			    },
			    out: function(){
			        $(".sub-menu:first",this)
			            .removeClass("hoverIn").toggleClass("hoverOut").fadeOut();
			    }
			});  
	  	},
	  	photoGallery: function()
	  	{
		  	var $slideshow = $(".voilivoilou-photo-gallery");
		  	
		  	if($slideshow.length)
		    {
		        $slideshow.each(function()
		        {
			        var $wrapper = $(this),
			        	wrap_count = $wrapper.attr("data-wrap-slides"),
						$container = $wrapper,
						$item = $(".slice-item", this);
		
		          $wrapper.addClass("slide-items-"+wrap_count);
		          $container.WrapThis(wrap_count, $item);
		
		        });
		        
		        //Callback
		        slideshow_8images();
		    }
	  	},
	  	swipeboxInit: function()
		{
			var $element = $("#page .swipebox");
			
			if($element.length)
			{
				$element.swipebox({
					hideBarsDelay: 20000
				});	
			}	
		},
		clientsSlideshow: function()
	  	{
		  	var $slideshow = $("#clients-feat .slideshow");
		  	
		  	if($slideshow.length)
		    {
		        $slideshow.slick({
			  		autoplay: false,
			  		slide: ".slide",
			  		autoplaySpeed:4000,
			  		arrows: true,
			  		dots: true,
			  		infinite:false,
			  		slidesToShow: 1,
			  		slidesToScroll:1,
			  		pauseOnDotsHover: true,
			  		customPaging : function(slider, i) 
			  		{
				        return '<div class="dot dot-'+i+'"></div>';
				    }
		  		});
		    }
	  	},
	  	customSelectInit: function()
	  	{
			var $select = $("#page form .custom-select");
			
			if($select.length)
			{
				$select.customSelect();
			}  	
	  	},
	  	contactButton: function()
	  	{
		  	var $target = $("#contact-button"),
		  		$form = $("#inquiries-form"),
		  		$height = $form.find(".wrap").height(),
		  		$body = $("html, body"),
		  		$target_input = $("#site-location-url"),
		  		$location_path = window.location.pathname;
		  		
		  
		  	$target.click(function(e)
		  	{
			  	e.preventDefault();
			  	
			  	$(this).toggleClass("active");
			  	
			  	
			  	if($form.hasClass("active"))
			  	{
				  	$form.animate({ height: "0px" },600).promise().done(function()
				  	{
					  	$form.removeClass("active");
					});
			  	} else {
				  	$form.animate({ height: $height },600).promise().done(function()
				  	{
					  	var $pos = $form.offset().top-40;
					  	
					  	$form.addClass("active");
					  	
					  	
					  	$body.animate({ 
							scrollTop: $pos
						},800);
					});

			  	}
			  	
			  	return false;
		  	});
		  	
		  	//Get page location
		  	$target_input.val(' voilivoilou.fr'+$location_path);		  	
	  	
	  	},
	  	parallaxInit: function()
	  	{
		  	$(window).Scrollax();
	  	},
	  	preloader: function ()
	  	{
		  	//when window is fully load
  			$(window).load(function()
  			{
				$("body").addClass("body-loaded");
			});
	  	},
	  	visibleElement: function()
	  	{
		  	var $element = $("#page .visible-animation");
	
			if($element.length)
			{
				$(window).scroll(function(event) 
				{  
				  $element.each(function(i, el) 
				  {
				    var el = $(el);
				    if (el.visible(true)) 
				    {
				      el.addClass("css-animation"); 
				    } 
				  });
				  
				});		
			}
	  	},
	  	hoverTouch: function()
	  	{
		  	var $element =  $("#page").find(".hover-touch");
	
			if($element.length)
			{
				$element.on("touchstart",function()
				{
				   $(this).toggleClass("touch");
				   
				});
				
				$element.on("touchend",function()
				{
				   $(this).toggleClass("touch");
				});
			}
	  	},
	  	lazyLoad:function()
	  	{
		  	var $element = $("#content").find(".lazyload-init");
		  	
			if($element.length)
			{
				$element.each(function()
				{
					$(this).show().lazyload({
						effect: "fadeIn"
					});		
				})
			}
			
			slidelazyLoad();
	  	},
	  	yachtCarousel: function()
	  	{
		  	var $element = $("#page .yachts-carousel");
		  	
			if($element.length)
		    {
		        $element.slick({
			  		autoplay: false,
			  		slide: ".slide",
			  		autoplaySpeed:4000,
			  		arrows: true,
			  		dots: false,
			  		infinite:false,
			  		slidesToShow: 3,
			  		slidesToScroll: 1,
			  		pauseOnDotsHover: true,
			  		responsive: [
				    {
				      breakpoint: 600,
				      settings: {
				        slidesToShow: 2,
				        slidesToScroll: 2
				      }
				    }
				    ]
		  		});
		  		
		  		slidelazyLoad();
		  		
		  		//Load images
		  		$element.on('afterChange', function(event)
		  		{		
			  		slidelazyLoad();			   
				});
		    }
	  	},
	  	galleryOne: function()
	  	{
		  	var $element = $("#page .voilivoilou-slideshow-one");
		  	
			if($element.length)
		    {
		        $element.slick({
			  		autoplay: false,
			  		slide: ".slide-item",
			  		autoplaySpeed:4000,
			  		arrows: false,
			  		dots: true,
			  		infinite:false,
			  		slidesToShow: 1,
			  		slidesToScroll: 1,
			  		pauseOnDotsHover: true,
			  		customPaging : function(slider, i) 
			  		{
				        return '<div class="dot dot-'+i+'"></div>';
				    }
		  		});
		  		
		  		slidelazyLoad();
		  		
		  		//Load images
		  		$element.on('afterChange', function(event)
		  		{		
			  		slidelazyLoad();			   
				});
		    }
	  	},
	  	galleryTwo: function()
	  	{
		  	var $element = $("#page .voilivoilou-slideshow-two");
		  	
			if($element.length)
		    {
		        $element.slick({
			  		autoplay: false,
			  		slide: ".slide-item",
			  		autoplaySpeed:4000,
			  		arrows: true,
			  		dots: true,
			  		infinite:true,
			  		slidesToShow: 1,
			  		slidesToScroll: 1,
			  		pauseOnDotsHover: false,
			  		adaptiveHeight: false,
			  		lazyLoad: 'ondemand',
			  		customPaging : function(slider, i) 
			  		{
				        return '<div class="dot dot-'+i+'"></div>';
				    }
		  		});
		  		
		  		//$element.find(".slick-list").css("height","600px");
		  		
		    }
	  	},
	  	carouselTwo: function()
	  	{
		 	 var $element = $("#page .slideshow.carousel-two-slides");
		  	
			if($element.length)
		    {
		        $element.slick({
			  		autoplay: false,
			  		slide: ".slide-item",
			  		autoplaySpeed:5000,
			  		arrows: true,
			  		dots: false,
			  		infinite:false,
			  		slidesToShow: 2,
			  		slidesToScroll: 1,
			  		pauseOnDotsHover: true,
			  		adaptiveHeight: false,
			  		lazyLoad: 'progressive'
		  		});
		  		
		  		
		    }
	  	},
	  	formSelect: function()
	  	{
		  	var $form = $("#inquiries-form"),
		  		$depart = $form.find("#select-depart"),
		  		$depart_first_choice = $depart.html(),
		  		$destination = $form.find("#select-dest");
		  	
		  	
		  	$destination.change(function () 
		  	{
			    
			    var $this = $(this),
			    	$index = $this.prop('selectedIndex'),
			    	$id_str = "#select-port-"+$index,
			    	$target = $($id_str);
			    	
			    	if($this.val()=="")
			    	{
				    	$depart.html($depart_first_choice);
				    	
				    	$depart.find('option:first').prop('selected', true).change();
			    	} else {
				    	$depart.html($target.html());
				    	
				    	$depart.find('option:first').prop('selected', true).change();
			    	}
			    	
			});
	  	}
	  	
  	}; 	
  	return events;
  };
  
  
  /* FN: DESTINATIONS 
  --------------------------------------------------------------------------------------------------------------------*/
  voilivoilou.flexibleContent = function()
  {
  	var events = {
	  	
	  	contentSlideShow: function()
	  	{
		  	var $slideshow = $("#main-content-slideshow");
		  	
			if($slideshow.length)
			{		 
				$slideshow.imagesLoaded(function() 
				{
					  
				    $slideshow.slick({
				  		autoplay: false,
				  		slide: ".slide",
				  		fade: true,
				  		autoplaySpeed: 4000,
				  		arrows: true,
				  		dots: true,
				  		infinite: false,
				  		slidesToShow: 1,
				  		slidesToScroll: 1,
				  		pauseOnDotsHover: true,
				  		adaptiveHeight: true,
				  		lazyLoad: 'progressive',
				  		appendArrows: $(".append-arrows"),
				  		appendDots: $(".append-dots"),
				  		customPaging : function(slider, i) 
				  		{
					        return '<div class="dot dot-'+i+'"></div>';
					    }
			  		});
			  		
			  		//Width
			  		var _width = $slideshow.find(".slick-dots").width();
			  		
			  		$slideshow.find(".dots-nav .wrap").css('width', _width);
			  		
			  		if($slideshow.find(".slide").length == 1 )
			  		{
				  		$slideshow.addClass("one-slide");
			  		}
			  		
			  		//Scroll to top on afterchange
					$slideshow.on('afterChange', function(event, slick, currentSlide, nextSlide)
					{
					  var $scroll_to = $slideshow.offset().top-45;
					  
					  $('html,body').animate({
					        scrollTop: $scroll_to
					  }, 900);
					 
					});
				}); 		
			}	
	  	},
	  	faq: function()
	  	{
		  	var $faq = $(".faq-wrapper");
		  	
		  	$faq.find(".faq-title").click(function()
		  	{
			  	$faq.find(".faq-content.active").slideUp();
			  	$faq.find(".active").removeClass("active");
			  	
			  	
			  	var $target = $(this).parents(".faq-wrapper");
			  	
			  	if($(this).hasClass("active"))
			  	{
				  	//$target.find(".faq-content").removeClass("active").slideUp();
				  	//$(this).removeClass("active");
			  	} else {
				  	$target.find(".faq-content").addClass("active").slideDown();
				  	$(this).addClass("active");
			  	}
		  	});
	  	}
	  	
  	}; 	
  	return events;
  };
  
  
   /* FN: HOMEPAGE
  --------------------------------------------------------------------------------------------------------------------*/
  voilivoilou.homepage = function()
  {
  	var events = {
	  	
	  	homeSlideshow: function()
  		{
	  		var $slider = $("#hero-slideshow .slideshow");
	  				  		
	  		if($slider.length)
	  		{		  		
		  		$slider.slick({
			  		autoplay: true,
			  		slide: ".slick-slide",
			  		autoplaySpeed:8000,
			  		arrows: true,
			  		dots: false,
			  		infinite: true,
			  		slidesToShow: 1,
			  		slidesToScroll:1,
			  		pauseOnDotsHover: true
		  		});
	  		}
	  	}
  	}; 	
  	return events;
  };
  
  
  
  /* Shortcodes: Ajax pagination
----------------------------------------------------------------*/
var ajaxLoadMorePosts = (function()
{
	var $pagination = $("#blog-pagination:not(.loadmore-deactivated)"),
		$nextPosts = $pagination.find("a"),
		lastPost = $("#blog-posts .posts-row:last"),
		$page = "";
	
	$nextPosts.click(function( e ) 
	{
		e.preventDefault();
		
			//Show proloader
			$pagination.toggleClass("active");
				
			$.ajax({
				url: $(this).attr("href"),
				success: function(data, textStatus, jqXHR) 
				{
					var $data = $(renderHtml(data)),
						$get_posts = $data.find("#blog-posts .posts-row").html(),
						$link =  $data.find("#blog-pagination > a");
						
					lastPost.after('<div class="posts-row appended">'+$get_posts+'</div>');
					
					($link.length)? $("#blog-pagination > a").attr("href", $link.attr("href")) : $pagination.hide();
					
				},
				error: function( jqXHR, textStatus, errorThrown )
				{
	                    alert( 'The following error occured: ' + textStatus +' Try to refresh the page or contact us. Error' + errorThrown);   
	            },
	            complete: function( jqXHR, textStatus )
                {
					
					
					$("#blog-posts .posts-row:last").find(".entry-content .lazyload-init").show().lazyload({
						effect: "fadeIn"
					});	
					
					$pagination.toggleClass("active");
					
					callbackSlieshow($("#blog-posts .posts-row:last"));
	            }
			});	
		
	});
});

  

  /* DOM Ready || Shoot everyone 
  ===============================================================================*/
  jQuery(document).ready(function()
  {	 
	 // GLobal
	 voilivoilou.onLoadBasics().mainNav();
	 voilivoilou.onLoadBasics().mobileNav();
	 voilivoilou.onLoadBasics().mainNav();
	 voilivoilou.onLoadBasics().swipeboxInit();
	 voilivoilou.onLoadBasics().customSelectInit();
	 voilivoilou.onLoadBasics().contactButton();
	 voilivoilou.onLoadBasics().parallaxInit();
	 voilivoilou.onLoadBasics().preloader();
	 voilivoilou.onLoadBasics().visibleElement();
	 voilivoilou.onLoadBasics().hoverTouch();
	 voilivoilou.onLoadBasics().lazyLoad();
	 voilivoilou.onLoadBasics().formSelect();
	 
	 scrollDownHeader();
	 
	 if($("#page .slideshow").length)
	 {
	 	voilivoilou.onLoadBasics().galleryOne();
	 	voilivoilou.onLoadBasics().galleryTwo();
	 	voilivoilou.onLoadBasics().yachtCarousel();
	 	voilivoilou.onLoadBasics().clientsSlideshow();
	 	voilivoilou.onLoadBasics().photoGallery();
	 	voilivoilou.onLoadBasics().carouselTwo();
	 }
	 
	 //Home
	 if($("body.home").length)
	 {
		 voilivoilou.homepage().homeSlideshow();
	 }
	
	 // Flexible Content
	 if($("#main-content-slideshow").length)
	 {
		 voilivoilou.flexibleContent().contentSlideShow();
	 }
	 
	 // Faq
	 if($("#flexible-content .faq-content").length)
	 {
		 voilivoilou.flexibleContent().faq();
	 }

	 
	 // Flexible Content
	 if($("body.page-template-page-blog").length)
	 {
		 ajaxLoadMorePosts();
	 }
	 
  });




/* Render html
--------------------------------------------------------------------------------------------------------*/
var renderHtml = function(html)
{	
	var result = String(html).replace(/<\!DOCTYPE[^>]*>/i, '')
							 .replace(/<(html|head|body|title|script)([\s\>])/gi,'<div id="get-html-$1"$2')
							 .replace(/<\/(html|head|body|title|script)\>/gi,'</div>');
	
	return result;
};


/*----------------------------------------------------------------
		Home lazy slide - Home Slideshoe
----------------------------------------------------------------*/
var lazy_slide = (function($slider)
{
	  var active = $slider.slick('slickCurrentSlide');	  
	  var activeSlide = $slider.find(".slick-slide").eq(active);
	  var _target = activeSlide.find(".slide-img");
	  
	  
	  if(!_target.hasClass("loaded"))
	  {
		  _target.css("background-image","url(" + _target.attr("data-image") + ")").addClass("loaded");
	  }	
});

/*----------------------------------------------------------------
		lazy slide - Home Slideshoe
----------------------------------------------------------------*/
var slidelazyLoad = (function()
{
	var $slide_element = $("#page .lazy-slides .slick-active:not(.lazy-loaded)");
	var $target_img = $slide_element.find(".slide-lazyload-init");
		  	
  	if($slide_element.length)
	{		
		$target_img.show().lazyload({
			effect: "fadeIn"
		});	
		
		$slide_element.addClass("lazy-loaded");
	}
});

/*----------------------------------------------------------------
		Photo Gallery
----------------------------------------------------------------*/
var slideshow_8images = (function()
{
	var $gallery = $("#page .voilivoilou-photo-gallery");
		
	if($gallery.length)
	{		  		
  		$gallery.slick({
	  		autoplay: false,
	  		slide: ".slide",
	  		autoplaySpeed:4000,
	  		arrows: true,
	  		dots: false,
	  		infinite:false,
	  		slidesToShow: 1,
	  		slidesToScroll:1,
	  		pauseOnDotsHover: true
  		});
  		
  		slidelazyLoad();
  		
  		//Load images
  		$gallery.on('afterChange', function(event)
  		{		
	  		slidelazyLoad();			   
		});
	}	
});

/*----------------------------------------------------------------
		Callback slideshows
----------------------------------------------------------------*/

var callbackSlieshow = (function($target)
{
  	var $element1 = $target.find(".voilivoilou-slideshow-one");
  	
	if($element1.length)
    {
        $element1.slick({
	  		autoplay: false,
	  		slide: ".slide-item",
	  		autoplaySpeed:4000,
	  		arrows: false,
	  		dots: true,
	  		infinite:false,
	  		slidesToShow: 1,
	  		slidesToScroll: 1,
	  		pauseOnDotsHover: true,
	  		customPaging : function(slider, i) 
	  		{
		        return '<div class="dot dot-'+i+'"></div>';
		    }
  		});
  		
  		slidelazyLoad();
  		
  		//Load images
  		$element1.on('afterChange', function(event)
  		{		
	  		slidelazyLoad();			   
		});
	}
	
  	var $element2 = $target.find(".voilivoilou-slideshow-two");
  	
	if($element2.length)
    {
        $element2.slick({
	  		autoplay: false,
	  		slide: ".slide-item",
	  		autoplaySpeed:4000,
	  		arrows: true,
	  		dots: true,
	  		infinite:true,
	  		slidesToShow: 1,
	  		slidesToScroll: 1,
	  		pauseOnDotsHover: true,
	  		adaptiveHeight: true,
	  		lazyLoad: 'progressive',
	  		customPaging : function(slider, i) 
	  		{
		        return '<div class="dot dot-'+i+'"></div>';
		    }
  		});
    }
});


/*----------------------------------------------------------------
		Back to top
----------------------------------------------------------------*/
var touchBottom = (function($this)
{
	var $templ = $this.find("#template.work-detail"),
		$body = $("body");
	
	if($templ.length)
	{
		$(window).scroll(function()
		{
			var _height = $(document).height()-620;
			var w_height = $(window).height();
			var total = $(window).scrollTop()+w_height;
			
	       if(_height<total)
	       {
	           $body.addClass("touched-bottom");
	       } else {
		       $body.removeClass("touched-bottom");
	       }
	    });
	}
});


/*----------------------------------------------------------------
		Scrolling down header
----------------------------------------------------------------*/
var scrollDownHeader = (function($this)
{
	
	
		// Hide Header on on scroll down
		var didScroll;
		var lastScrollTop = 0;
		var delta = 5;
		var $body = $("body");
		var navbarHeight = $("#header").outerHeight();
		
		$(window).scroll(function(event){
		    didScroll = true;
		});
		
		setInterval(function() 
		{
		    if(didScroll) 
		    {
		        var st = $(this).scrollTop();
		    
			    // Make sure they scroll more than delta
			    if(Math.abs(lastScrollTop - st) <= delta)
			        return;
			    
			    // If they scrolled down and are past the navbar, add class .nav-up.
			    // This is necessary so you never see what is "behind" the navbar.
			    if (st > lastScrollTop && st > navbarHeight)
			    {
			        // Scroll Down
			         $body.addClass("scrolling-down");
			    } else {
			        // Scroll Up
			        if(st + $(window).height() < $(document).height()) 
			        {
			          $body.removeClass("scrolling-down");
			        }
			    }
			    
			    lastScrollTop = st;
			    
		        didScroll = false;
		    }
		}, 250);
	
	
});


/*----------------------------------------------------------------
		$.fn Visible element
----------------------------------------------------------------*/
$.fn.visible = function(partial) 
{
  var $t            = $(this),
      $w            = $(window),
      viewTop       = $w.scrollTop(),
      viewBottom    = viewTop + $w.height(),
      _top          = $t.offset().top,
      _bottom       = _top + $t.height(),
      compareTop    = partial === true ? _bottom : _top,
      compareBottom = partial === true ? _top : _bottom;

return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
};


/*-------------------------------------------------------------------------------
	$.fn WrapThis fn for Featured Resources
-------------------------------------------------------------------------------*/
$.fn.WrapThis = function(arg1, arg2) 
{
  var wrapClass = "slide";

  var itemLength = $(this).find(arg2).length;
  var remainder = itemLength%arg1;
  var lastArray = itemLength - remainder;

  var arr = [];

  if($.isNumeric(arg1))
  {
    $(this).find(arg2).each(function(idx, item) {
      var newNum = idx + 1;

      if(newNum%arg1 !== 0 && newNum <= lastArray){
        arr.push(item);
      }
      else if(newNum%arg1 == 0 && newNum <= lastArray) {
        arr.push(item);
        var column = $(this).pushStack(arr);
        column.wrapAll('<div class="' + wrapClass + '"/>');
        arr = [];
      }
        else if(newNum > lastArray && newNum !== itemLength){
          arr.push(item);
        }
        else {
          arr.push(item);
          var column = $(this).pushStack(arr);
          column.wrapAll('<div class="' + wrapClass + '"/>');
          arr = []
        }
    });
  }
}




})(jQuery);

	