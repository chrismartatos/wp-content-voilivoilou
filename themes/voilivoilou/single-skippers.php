<?php get_header(); ?>
	
<?php 
	$imgsrc = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full");
    $getimg = $imgsrc[0];
?>
<div id="content" class="page-<?php print $post->post_name; ?>">
	
		<div class="skipper-header">
			<div class="container">
				<div class="row">
					<div class="col-lg-2"></div>
					<div class="col-lg-4">
						<img src="<?php echo $getimg; ?>" alt="skipper">
					</div>
					<div class="col-lg-4">
						<h1><?php the_title(); ?></h1>
						<?php include('elements/description.php'); ?>
						<div class="entry-content">
						<?php the_content(); ?>	
						</div>
					</div>
					<div class="col-lg-2"></div>
				</div>
			</div>
		</div>
			
		<?php
			//Flexible content 
			include('elements/flexible-content.php'); 
		?>
		
		<section class="voili-feat bg-grey">
			<?php $skipper_global_title = get_field("skippers_global_title","option"); ?>
			<h3 class="feat-title"><?php echo $skipper_global_title; ?></h3>
			
			<?php echo do_shortcode('[skippers]'); ?>
		</section>
		
	<?php if( have_posts() ): the_post(); ?>
	
	<?php else: ?>
		<p>Sorry, this page not longer exists.</p>
	<?php endif; ?>
	
</div><!--END #content -->

<?php get_footer(); ?>
