<?php
/*
Template Name: Template - Boats single
*/
?>

<?php get_header(); ?>

<div id="intro-header" class="cf">

	<h1 class="page-title"><?php the_title(); ?></h1>

</div>

<div id="content" class="page-<?php print $post->post_name; ?>">

		<?php include('elements/description.php'); ?>

		<div class="voili-feat bg-white remove-padding-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>

		<?php

        // check if the repeater field has rows of data
        if (have_rows('yacht_details')):

            // loop through the rows of data
            while (have_rows('yacht_details')) : the_row();

            $yacht_details_t = get_sub_field('title');
            $yacht_details_1 = get_sub_field('column_1');
            $yacht_details_2 = get_sub_field('column_2');
            $yacht_details_3 = get_sub_field('column_3');

            ?>

		    <section class="voili-feat yacht-details">
			    <h3 class="feat-title"><?php echo $yacht_details_t; ?></h3>
				<div class="container entry-content">
					<div class="row">
						<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 grid-item"><?php echo $yacht_details_1; ?></div>
						<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 grid-item"><?php echo $yacht_details_2; ?></div>
						<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 grid-item"><?php echo $yacht_details_3; ?></div>
					</div>
				</div>
		    </section>

			<?php
            endwhile;

        endif;
        ?>


		<?php
            //Flexible content
            include('elements/flexible-content.php');
        ?>

		<?php
            //blocks
            include('elements/blocks.php');
        ?>

	<?php if (have_posts()): the_post(); ?>

	<?php else: ?>
		<p>Sorry, this page not longer exists.</p>
	<?php endif; ?>

</div><!--END #content -->

<?php get_footer(); ?>
