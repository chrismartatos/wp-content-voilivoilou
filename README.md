# PROJECT VoiliVoilou

This is the repo for VoiliVoilou's wp-content base folder.

## Getting Started (For DEVs) - Local Machine tools

You can use MAMP for local environment.

### Setting up your local environment

The following steps outline the process for setting up your local environment

#### First Local environment using XAMP, MAMP or Vagrant

The following steps will get Variable Vagrant Vagrants (VVV), Variable VVV (vv), and VVV dashboard running on your computer.

#### IMPORTANT .gitignore

Ignore Uploads, Plugins, Cache folders etc. Keep only theme in the repo
